<?php

require_once 'sqlhelper.php';

$conn = connectToMyDatabase();

if($conn){
         $st = $_POST["serverTime"];
          $newMessagesResult = $conn->query("call get_new_messages(\"$st\")");
          clearConnection($conn);

          if(!empty($newMessagesResult)){

               $newMessagesTable = $newMessagesResult->fetch_all(MYSQLI_ASSOC);

               $output = array();

               foreach ($newMessagesTable as $newMessage){

                    array_push($output,array(
                         "timesent"=> $newMessage["Timesent"],
                         "sender"=> $newMessage["Sender"],
                         "message"=> $newMessage["Message"]));
               }
               echo json_encode($output);
          }

}else {

     echo "ERROR!";
}



?>
