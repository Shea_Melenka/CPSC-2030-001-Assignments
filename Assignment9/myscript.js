//Addapted from week11 ajax example
let lastTime = getCurrentDateTime();
console.log(lastTime);
$(document).ready(function() {

     let messageHistory = $(".messageBacklog");

     $.ajax({
        url: "loadInitialMessages.php",
        method: "POST",
        data: {
            serverTime: getCurrentDateTime(),
        },
        success: function(msg){
           let data = JSON.parse(msg);

           for(i = data.length-1; i >= 0; i--) {
                messageHistory.append("<div class='messageInstance'><div class='timesent'>"+data[i].timesent+"</div><div class='message'>"+data[i].message+"</div><div class='sender "+data[i].sender+"'>"+data[i].sender+"</div></div>");
                messageHistory.append("</br>");

                lastTime = data[i].timesent;

                let thisUser = data[i].sender;
                let thisUserNum = -1;
                let newUser = true;
                for(n = 0; n < userList.length; n++){
                    if(userList[n] == thisUser){
                         newUser = false;
                         thisUserNum = n;
                    }
               }

               if(newUser){
                   userList[userList.length] = thisUser;
                   $("."+thisUser).parent().css({"background-color": colourScheme[userList.length-1][0], "border-color": colourScheme[userList.length-1][1]});
              }else{
                   $("."+thisUser).parent().css({"background-color": colourScheme[thisUserNum][0], "border-color": colourScheme[thisUserNum][1]});
              }

           }

        }
    })

    refreshFunction();

    function refreshFunction() {

         $.ajax({
           url: "updateMessages.php",
           method: "POST",
           data: {
               serverTime: lastTime
          }
          })
           .done(function(msg){
                if(msg){
                   let data = JSON.parse(msg);

                   for(i = 0; i < data.length; i++) {

                        messageHistory.append("<div class='messageInstance'><div class='timesent'>"+data[i].timesent+"</div><div class='message'>"+data[i].message+"</div><div class='sender "+data[i].sender+"'>"+data[i].sender+"</div></div>");
                        messageHistory.append("</br>");

                        lastTime = data[i].timesent;

                        let thisUser = data[i].sender;
                        let thisUserNum = -1;
                        let newUser = true;
                        for(n = 0; n < userList.length; n++){
                            if(userList[n] == thisUser){
                                 newUser = false;
                                 thisUserNum = n;
                            }
                       }

                       if(newUser){
                           userList[userList.length] = thisUser;
                           $("."+thisUser).parent().css({"background-color": colourScheme[userList.length-1][0], "border-color": colourScheme[userList.length-1][1]});
                      }else{
                           $("."+thisUser).parent().css({"background-color": colourScheme[thisUserNum][0], "border-color": colourScheme[thisUserNum][1]});
                      }

                   }

              }


         })
         .always(function(){
              setTimeout(refreshFunction, 1000);
         });

    }

    let sendMessage = $(".sendMessage");
    sendMessage.click(function(){

         let currentTime = getCurrentDateTime();
         let currentUser = $(".usernameField").val();
         let userMessage = $(".compositionBox").val();

         $.ajax({
            url: "addMessage.php",
            method: "POST",
            data: {
                serverTime: currentTime,
                from: currentUser,
                messageBody:userMessage
            },
            success: function(){

                 lastTime = currentTime;

                messageHistory.append("<div class='messageInstance'><div class='timesent'>"+currentTime+"</div><div class='message'>"+userMessage+"</div><div class='sender "+currentUser+"'>"+currentUser+"</div></div>");
                messageHistory.append("</br>");

                 let currentUserNum = -1;
                 let newUser = true;
                 for(n = 0; n < userList.length; n++){
                    if(userList[n] == currentUser){
                         newUser = false;
                         currentUserNum = n;
                    }
               }

               if(newUser){
                    console.log("new user");
                    userList[userList.length] = currentUser;
                    $("."+currentUser).parent().css({"background-color": colourScheme[userList.length-1][0], "border-color": colourScheme[userList.length-1][1]});
               }else{
                    $("."+currentUser).parent().css({"background-color": colourScheme[currentUserNum][0], "border-color": colourScheme[currentUserNum][1]});
               }


            }
        })

    });

});

function testForNewUser(currentUser){
     let newUser = true;
     for(i = 0; i < userList.length; i++){
         if(userList[i] == currentUser){
              newUser = false;
         }
    }

     if(newUser){
          console.log("new user");
          userList[userList.length] = currentUser;
          $("."+currentUser).parent().css({"background-color": colourScheme[userList.length-1][0], "border-color": colourScheme[userList.length-1][1]});
     }

}

function getCurrentDateTime(){
     //Time conversion code borrowed from https://www.codexworld.com/how-to/get-current-date-time-using-javascript/
     let today = new Date();
     let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
     let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
     let dateTime = date+' '+time;

     return dateTime;
}

let colourScheme = [["#ff6666", "#e60000"], ["#ff8c66", "#e63900"], ["#ffb366", "#e67300"], ["#ffd966", "#e6ac00"], ["#ffff66", "#e6e600"], ["#d9ff66", "#ace600"], ["#b3ff66", "#73e600"], ["#8cff66", "#39e600"], ["#66ff66", "#00e600"], ["#66ff8c", "#00e639"], ["#66ffb3", "#00e673"], ["#66ffd9", "#00e6ac"], ["#66ffff", "#00e6e6"], ["#66d9ff", "#00ace6"], ["#66b3ff", "#0073e6"], ["#668cff", "#0039e6"], ["#6666ff", "#0000e6"], ["#8c66ff", "#3900e6"], ["#b366ff", "#7300e6"], ["#d966ff", "#ac00e6"], ["#ff66ff", "#e600e6"], ["#ff66d9", "#e600ac"], ["#ff66b3", "#e60073"], ["#ff668c", "#e60039"], ["#ff6666", "#e60000"]];
let userList = [];
