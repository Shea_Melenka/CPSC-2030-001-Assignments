CREATE TABLE `message_table` (
  `Timesent` varchar(19) DEFAULT NULL,
  `Sender` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL
);

INSERT INTO `message_table` (`Timesent`, `Sender`, `Message`) VALUES
(DATE_ADD(NOW(), INTERVAL -800 MINUTE), 'Severide', 'Shay are you almost ready to go?'),
(DATE_ADD(NOW(), INTERVAL -47970 SECOND), 'Severide', 'I want to stop by the pankcake diner before work'),
(DATE_ADD(NOW(), INTERVAL -795 MINUTE), 'Dawson', 'Oh my god! Pick me up some chocolate chip pancakes with the peanut butter topping and whip cream PLEASE'),
(DATE_ADD(NOW(), INTERVAL -794 MINUTE), 'Shay', 'I will. I\'ll be there in one minute Severide'),
(DATE_ADD(NOW(), INTERVAL -793 MINUTE), 'Dawson', 'YES!'),
(DATE_ADD(NOW(), INTERVAL -792 MINUTE), 'Severide', 'You are one lucky lady Dawson'),

(DATE_ADD(NOW(), INTERVAL -30 MINUTE), 'Shay', 'Hey Dawson, what time are you coming over tonight?'),
(DATE_ADD(NOW(), INTERVAL -29 MINUTE), 'Severide', 'You didn\'t tell me that Dawson was coming over tonight'),
(DATE_ADD(NOW(), INTERVAL -28 MINUTE), 'Shay', 'Oh shoot, I meant to ask you if it was cool while we were at work today'),
(DATE_ADD(NOW(), INTERVAL -1650 SECOND), 'Shay', 'Is it alright if she comes over?'),
(DATE_ADD(NOW(), INTERVAL -26 MINUTE), 'Severide', 'Yeah, it\'s fine.'),
(DATE_ADD(NOW(), INTERVAL -25 MINUTE), 'Severide', 'Should I be prepared to head to Molly\'s so you two can have a girl\'s night?'),
(DATE_ADD(NOW(), INTERVAL -1470 SECOND), 'Dawson', 'What, you don\'t want to join us for a delicious dinner, too much wine, and a trashy movie?'),
(DATE_ADD(NOW(), INTERVAL -23 MINUTE), 'Severide', 'No thanks, I\'d rather drink with 51 at Molly\'s'),
(DATE_ADD(NOW(), INTERVAL -22 MINUTE), 'Dawson', 'I\'ll be over at about 5pm'),
(DATE_ADD(NOW(), INTERVAL -21 MINUTE), 'Shay', 'Okay, sounds good'),

(DATE_ADD(NOW(), INTERVAL 60 SECOND), 'Kidd', 'I\'ll meet you at Molly\'s Severide'),
(DATE_ADD(NOW(), INTERVAL 70 SECOND), 'Casey', 'I\'m going to be heading to New York with Naomi for a day so I will see everyone when I get back.'),
(DATE_ADD(NOW(), INTERVAL 80 SECOND), 'Boden', 'Let me know what you find in New York Casey. This investigation has become a big deal to OFI'),
(DATE_ADD(NOW(), INTERVAL 90 SECOND), 'Casey', 'Will do Chief'),
(DATE_ADD(NOW(), INTERVAL 100 SECOND), 'Boden', 'Truck will have a stand in Captain for the one shift that Casey is away so be aware 51.'),
(DATE_ADD(NOW(), INTERVAL 110 SECOND), 'Otis', 'Sounds good Chief.'),
(DATE_ADD(NOW(), INTERVAL 120 SECOND), 'Mouch', 'We\'ll be on our best behaviour.'),
(DATE_ADD(NOW(), INTERVAL 130 SECOND), 'Casey', 'Thank you Chief'),

(DATE_ADD(NOW(), INTERVAL 300 SECOND), 'Herrmann', 'Remember to wear your jerseys tonight, it\'s hockey night!'),
(DATE_ADD(NOW(), INTERVAL 330 SECOND), 'Kidd', 'Really Herrmann, do we have to?'),
(DATE_ADD(NOW(), INTERVAL 360 SECOND), 'Herrmann', 'YES, it\'s hockey night! You can\'t work at Molly\'s tonight if you don\'t wear your jersey.'),
(DATE_ADD(NOW(), INTERVAL 390 SECOND), 'Otis', 'The Blackhawks are playing the Bruins tonight, so it\'s a big deal'),
(DATE_ADD(NOW(), INTERVAL 420 SECOND), 'Kidd', 'I disagree but whatever, I\'ll wear my jersey'),
(DATE_ADD(NOW(), INTERVAL 450 SECOND), 'Herrmann', 'Good. And all drinks are 15% off for hockey night so be ready for a crowd.');
COMMIT;
