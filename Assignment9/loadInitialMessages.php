<?php

require_once 'sqlhelper.php';

$conn = connectToMyDatabase();

if($conn){

     $initialMessageLoadResult = $conn->query("call initial_message_load()");
     clearConnection($conn);

     $initialMessageTable = $initialMessageLoadResult->fetch_all(MYSQLI_ASSOC);

     $output = array();

     foreach ($initialMessageTable as $initialMessage){

          array_push($output,array(
               "timesent"=> $initialMessage["Timesent"],
               "sender"=> $initialMessage["Sender"],
               "message"=> $initialMessage["Message"]));
     }
     echo json_encode($output);


}else {

     echo "ERROR!";
}



?>
