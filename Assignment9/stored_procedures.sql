/* function to load last 10 messages within the hour*/
DELIMITER //
CREATE PROCEDURE initial_message_load()
BEGIN

     SELECT *
     FROM message_table
     WHERE Timesent < NOW()
     AND Timesent > DATE_ADD(NOW(), INTERVAL -1 HOUR)
     ORDER BY Timesent DESC
     LIMIT 10;

END //
DELIMITER ;

/* function to add a message to the server*/
DELIMITER //
CREATE PROCEDURE add_message
(IN timesent TIMESTAMP, IN sender text, IN message text)
BEGIN

     INSERT INTO message_table (Timesent, Sender, Message)
     VALUES (timesent, sender, message);
     ALTER TABLE message_table
     ORDER BY Timesent ASC;

END //
DELIMITER ;

/* function to return all messages after a specified time*/
DELIMITER //
CREATE PROCEDURE get_new_messages
(IN timePoint TIMESTAMP)
BEGIN

     SELECT *
     FROM message_table
     WHERE Timesent < NOW()
     AND Timesent > timePoint;

END //
DELIMITER ;
