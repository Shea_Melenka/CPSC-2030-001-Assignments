<!--/*
	Assignment#6 CPSC2030
	Title: index.php
	Author: Shea Melenka
	Date: October 21st 2018
*/-->
<!--CODE BASED OFF OF WEEK7 TWIG EXAMPLES-->
<?php
     // Start the session
     session_start();

     //We are going to render a template, rather than
     //mix php and html.
     //What this means is that we will
     // -Load the file
     // -store it as a string
     // -Process the string, by replacing templated values
     // -echo the string
     require_once 'sqlhelper.php';
     require_once './vendor/autoload.php';  //include the twig library.
     $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory

     //Sometimes you have to manually delete the cache
     $twig = new Twig_Environment($loader);

     //Sql setup

     $conn = connectToMyDatabase();


     //Simple example of routing
     //I can load a different page, if there is an error
     if($conn){

          //setup twig
          $template = $twig->load('headermaintitle.twig.html');

          //call render to replace values in template with ones specified in my array
          //Since the return value is a string, I can echo it.
          $dbname = "Pokedex";
          echo $template->render(array("dbname"=>$dbname));

     }else {

          //One benefit is that we can load a full error page
          $template = $twig->load("error.twig.html");
          echo $template->render(array("message"=>"Title query failed"));
     }


     //POPULAR POKEMONS SIDEBAR SECTION (pokemon's selected from https://pokemongo.gamepress.gg/popular-pokemon-all?page=0 based on October 28th 2018 popular Pokemon. Screenshots avaliable in PopularPokemonScreenshot.docx)
     $result1 = $conn->query("call popular()");
     clearConnection($conn);

     if($result1){
          $popularList = $result1->fetch_all(MYSQLI_ASSOC);

          $linkArray = array();
          $count = 0;
          foreach ($popularList as $popularPokemon){

               $link = "index.php?name=".urlencode($popularPokemon["Name"]);
               $linkArray[$popularPokemon["Name"]] = $link;
               $count++;
          }

          $template = $twig->load("sidebarmenu.twig.html");
          echo $template->render(array("popularList"=>$popularList, "linkArray"=>$linkArray));

     }else{
          $template = $twig->load("error.twig.html");
          echo $template->render(array("message"=>"Sidebar query failed"));
     }


     //POKEMON CARDS SECTION
     if(!isset($_SESSION['favouritePokemons'])) {
          $_SESSION['favouritePokemons'] = array();
     }

     $favouritePokemonRemoved = false;

     $favouritePokemonCount = sizeof($_SESSION['favouritePokemons']);
     if($favouritePokemonCount <= 7){

          if (!empty($_GET["fave"])) {

               $fave = mysqli_real_escape_string($conn, $_GET["fave"]);

               for ($x=0; $x < $favouritePokemonCount; $x++) {

                    if($_SESSION['favouritePokemons'][$x]['Name'] == $fave){

                         $favouritePokemonCount--;
                         $favouritePokemonRemoved = true;

                         unset( $_SESSION['favouritePokemons'][$x] );
                         $_SESSION['favouritePokemons'] = array_values($_SESSION['favouritePokemons']);
                    }
               }

               if(!$favouritePokemonRemoved && $favouritePokemonCount < 7){

                    $result3 = $conn->query("call pokedex_card(\"$fave\")");
                    clearConnection($conn);


                    if($result3){

                         $newFave = $result3->fetch_assoc();
                         $_SESSION['favouritePokemons'][$favouritePokemonCount] = $newFave;

                    }else{
                         $template = $twig->load("error.twig.html");
                         echo $template->render(array("message"=>"Card query failed"));
                    }

               }

          }

     }

     $template = $twig->load("pokemoncard.twig.html");
     echo $template->render(array("pokefave"=> $_SESSION['favouritePokemons']));


     //POKEMON TABLE SECTION
     $result = $conn->query("call pokedex_table()");
     clearConnection($conn);

     if($result){
          $poketable = $result->fetch_all(MYSQLI_ASSOC);

          $template = $twig->load("pokemontable.twig.html");
          echo $template->render(array("poketable"=>$poketable, "pokefave"=> $_SESSION['favouritePokemons']));

     }else{
          $template = $twig->load("error.twig.html");
          echo $template->render(array("message"=>"Table query failed"));
     }


     $conn->close(); //clean up connection
            //Probably autoclose on exit, but just to be safe.
            //I really wish I had RAII here.
?>
<!-- no more html here -->
<!-- leads to cleaner code -->
