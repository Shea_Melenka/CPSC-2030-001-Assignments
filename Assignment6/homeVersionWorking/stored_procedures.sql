/* function to get the everything about entire pokedex*/
DELIMITER //
CREATE PROCEDURE pokedex_table()
BEGIN

     SELECT ID, Name, Type
     FROM pokemon;

END //
DELIMITER ;

/* function to get the entire pokedex*/
DELIMITER //
CREATE PROCEDURE pokedex_card
(IN str text)
BEGIN

     SELECT *
     FROM pokemon
     WHERE str = pokemon.Name;

END //
DELIMITER ;

/* function to get part of the pokedex based on type*/
DELIMITER //
CREATE PROCEDURE pokedex_type
(IN str text)
BEGIN

     SELECT DISTINCT pokemon.ID, pokemon.Name, pokemon.Type
     FROM pokemon
     LEFT JOIN two_types
     ON pokemon.Name = two_types.Name
     WHERE str = pokemon.Type
     OR str = two_types.Type1
     OR str = two_types.Type2
     ORDER BY pokemon.ID;

END //
DELIMITER ;

/*One type*/
DELIMITER //
CREATE PROCEDURE pokemon_type
(IN str text)
BEGIN
     SELECT Type FROM pokemon
     WHERE NOT EXISTS (SELECT Name FROM two_types
     WHERE str = two_types.Name);
END //
DELIMITER ;

/*one type*/
DELIMITER //
CREATE PROCEDURE single_type
(IN str text)
BEGIN

     SELECT Type
     FROM pokemon
     Left JOIN two_types
     ON pokemon.Name = two_types.Name
     WHERE two_types.Name IS NULL
     AND pokemon.Name = str;

END //
DELIMITER ;

/*Two types*/
DELIMITER //
CREATE PROCEDURE multi_type
(IN str text)
BEGIN

     SELECT Type1, Type2
     FROM two_types
     WHERE Name = str;

END //
DELIMITER ;

/*Get information in the pokemon table*/
DELIMITER //
CREATE PROCEDURE pokemon_data
(IN str text)
BEGIN
     SELECT *
     FROM pokemon
     WHERE Name = str;

END //
DELIMITER ;

/*Get information from strong_against table*/
DELIMITER //
CREATE PROCEDURE strong_against_data
(IN str text)
BEGIN
     SELECT Normal, Fighting, Flying, Poison, Ground, Rock, Bug, Ghost, Steel, Fire, Water, Grass, Electric, Psychic, Ice, Dragon, Fairy, Dark
     FROM strong_against
     WHERE Type = str;

END //
DELIMITER ;

/*Get information from weak_against table*/
DELIMITER //
CREATE PROCEDURE weak_against_data
(IN str text)
BEGIN
     SELECT Normal, Fighting, Flying, Poison, Ground, Rock, Bug, Ghost, Steel, Fire, Water, Grass, Electric, Psychic, Ice, Dragon, Fairy, Dark
     FROM weak_against
     WHERE Type = str;

END //
DELIMITER ;

/*Get information from resistant_to table*/
DELIMITER //
CREATE PROCEDURE resistant_to_data
(IN str text)
BEGIN
     SELECT Normal, Fighting, Flying, Poison, Ground, Rock, Bug, Ghost, Steel, Fire, Water, Grass, Electric, Psychic, Ice, Dragon, Fairy, Dark
     FROM resistant_to
     WHERE Type = str;

END //
DELIMITER ;

/*Get information from vulnerable_to table*/
DELIMITER //
CREATE PROCEDURE vulnerable_to_data
(IN str text)
BEGIN
     SELECT Normal, Fighting, Flying, Poison, Ground, Rock, Bug, Ghost, Steel, Fire, Water, Grass, Electric, Psychic, Ice, Dragon, Fairy, Dark
     FROM vulnerable_to
     WHERE Type = str;

END //
DELIMITER ;

/*Get names of popular pokemon*/
DELIMITER //
CREATE PROCEDURE popular()
BEGIN

     SELECT Name
     FROM popular;

END //
DELIMITER ;

/*Favourites*/
DELIMITER //
CREATE PROCEDURE create_favourites()
BEGIN
     CREATE TABLE IF NOT EXISTS favourites(
         ID int(3),
         Name varchar(28)
     );

END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE alter_favourites
(IN str text)
BEGIN

     IF str == favourites.Name THEN delete_favourites(str);

     ELSE add_favourites(str);

END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE alter_favourites
(IN str text)
BEGIN

     INSERT INTO favourites
     SELECT pokemon.ID, pokemon.Name
     FROM pokemon
     WHERE str = pokemon.Name
     AND favourites.Name IS NULL;

END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE get_favourites()
BEGIN

     SELECT pokemon.*
     FROM pokemon
     left JOIN favourites
     ON pokemon.Name = favourites.Name;

END //
DELIMITER ;
