<!--/*
	Assignment#5 CPSC2030
	Title: pokemon.php
	Author: Shea Melenka
	Date: October 21st 2018
*/-->
<!--CODE BUILT FROM PAGE2.PHP EXAMPLE-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <title>Pokedex</title>
</head>
<?php
//Setup
include "sqlhelper.php";
//include "stored_procedures.sql";
//database stuff first
   //SQL  portion
   $user = 'CPSC2030';
   $pwd = 'CPSC2030';
   $server = 'localhost';
   $dbname = 'pokedex';

   $conn = new mysqli($server, $user, $pwd, $dbname);
   $name = mysqli_real_escape_string($conn, $_GET["name"]);

   $result = $conn->query("call pokemon_data(\"$name\")");
   clearConnection($conn);

   $type_order = array("Normal", "Fighting", "Flying", "Poison", "Ground", "Rock", "Bug", "Ghost", "Steel", "Fire", "Water", "Grass", "Electric", "Psychic", "Ice", "Dragon", "Fairy", "Dark");

   function printLoop($input, $type_order) {
        $size = sizeof($input);

        for ($x = 0; $x < $size; $x++) {

             if($input[$x] == 1){

                  echo $type_order[$x]." ";
             }

        }

    }

$result_array = array();
    function arrayLoop($input, $type_order, $result_array) {
         $size = sizeof($input);
         $length = sizeof($result_array);

         for ($x = 0; $x < $size; $x++) {

              if($input[$x] == 1){

                   $result_array[$length] = $type_order[$x];
                   $length++;

              }

         }
         return $result_array;
     }

     function printArray($print){
          $length = sizeof($print);
         for ($x = 0; $x <= $length; $x++) {

              if (!empty($print[$x])) {
                  echo $print[$x]." ";
             }
         }
     }

?>
<body>
     <a href=pokedex.php><h2>Home</h2></a>
     <h1><?php echo $name; ?></h1>

     <?php
          if($result){

               $pokemon = $result->fetch_row();
               //REFERENCE $pokemon["0"]
               echo "<div class='body_text'>";
               echo "<div class='ID top_info'>$pokemon[0]</div>";
               echo "<div class='Type top_info'>$pokemon[2]</div>";

               echo "<div class='stats'>HP: $pokemon[3]</div>";
               echo "<div class='stats'>Attack: $pokemon[4]</div>";
               echo "<div class='stats'>Defense: $pokemon[5]</div>";
               echo "<div class='stats'>Special Attack: $pokemon[6]</div>";
               echo "<div class='stats'>Special Defense: $pokemon[7]</div>";
               echo "<div class='stats'>Speed: $pokemon[8]</div>";
               echo "<div class='stats'>Total Statistics: $pokemon[9]</div>";
               echo "</div>";

          } else {
               echo "Error";
          }

          //type set up pokemon with one type
          $result1 = $conn->query("call single_type(\"$name\")");
          clearConnection($conn);
          $poke_1_type = $result1->fetch_row();


          if (!empty($poke_1_type)) {

               //Strong aganinst section
               $strong_against = $conn->query("call strong_against_data(\"$poke_1_type[0]\")");
               clearConnection($conn);

               if($strong_against){

                    echo "<div class='strong'>Strong Against: ";

                    $strong = $strong_against->fetch_row();
                    printLoop($strong, $type_order);

                   echo "</div>";

               }

               //Weak aganinst section
               $weak_against = $conn->query("call weak_against_data(\"$poke_1_type[0]\")");
               clearConnection($conn);

               if($weak_against){

                    echo "<div class='weak'>Weak Against: ";

                    $weak = $weak_against->fetch_row();
                    printLoop($weak, $type_order);

                   echo "</div>";

               }

               //Resistant to section
               $resistant_to = $conn->query("call resistant_to_data(\"$poke_1_type[0]\")");
               clearConnection($conn);

               if($resistant_to){

                    echo "<div class='resistant'>Resistant To: ";

                    $resistant = $resistant_to->fetch_row();
                    printLoop($resistant, $type_order);

                   echo "</div>";

               }

               //Vulnerable to section
               $vulnerable_to = $conn->query("call vulnerable_to_data(\"$poke_1_type[0]\")");
               clearConnection($conn);

               if($vulnerable_to){

                    echo "<div class='vulnerable'>Vulnerable To: ";

                    $vulnerable = $vulnerable_to->fetch_row();
                    printLoop($vulnerable, $type_order);

                   echo "</div>";

               }

          }

          //type set up for pokemon with two types
          $result2 = $conn->query("call multi_type(\"$name\")");
          clearConnection($conn);
          $poke_2_type = $result2->fetch_row();

          if (!empty($poke_2_type)) {

              //Strong aganinst section
              $strong_against1 = $conn->query("call strong_against_data(\"$poke_2_type[0]\")");
              clearConnection($conn);
              $strong_against2 = $conn->query("call strong_against_data(\"$poke_2_type[1]\")");
              clearConnection($conn);

              if($strong_against1){

                   echo "<div class='strong'>Strong Against: ";

                   $strong = $strong_against1->fetch_row();
                   $result_array = arrayLoop($strong, $type_order, $result_array);
             }

             if($strong_against2){

                  $strong = $strong_against2->fetch_row();
                  $result_array = arrayLoop($strong, $type_order, $result_array);
            }

            //Array unique code taken from http://php.net/manual/en/function.array-unique.php
            $print = array_unique($result_array);
            printArray($print);
            echo "</div>";

            unset($result_array);
            $result_array = array();


            //Weak aganinst section
          $weak_against1 = $conn->query("call weak_against_data(\"$poke_2_type[0]\")");
          clearConnection($conn);
          $weak_against2 = $conn->query("call weak_against_data(\"$poke_2_type[1]\")");
          clearConnection($conn);

          if($weak_against1){

                 echo "<div class='weak'>Weak Against: ";

                 $weak = $weak_against1->fetch_row();
                 $result_array = arrayLoop($weak, $type_order, $result_array);
          }

          if($weak_against2){

               $weak = $weak_against2->fetch_row();
               $result_array = arrayLoop($weak, $type_order, $result_array);
         }

         //Array unique code taken from http://php.net/manual/en/function.array-unique.php
         $print = array_unique($result_array);
         printArray($print);
         echo "</div>";

         unset($result_array);
         $result_array = array();

              //Resistant to section
            $resistant_to1 = $conn->query("call resistant_to_data(\"$poke_2_type[0]\")");
            clearConnection($conn);
            $resistant_to2 = $conn->query("call resistant_to_data(\"$poke_2_type[1]\")");
            clearConnection($conn);

            if($resistant_to1){

                   echo "<div class='resistant'>Resistant Against: ";

                   $resistant = $resistant_to1->fetch_row();
                   $result_array = arrayLoop($resistant, $type_order, $result_array);
            }

            if($resistant_to2){

                 $resistant = $resistant_to2->fetch_row();
                 $result_array = arrayLoop($resistant, $type_order, $result_array);
           }

           //Array unique code taken from http://php.net/manual/en/function.array-unique.php
           $print = array_unique($result_array);
           printArray($print);
           echo "</div>";

           unset($result_array);
           $result_array = array();

                //vulnerable to section
             $vulnerable_to1 = $conn->query("call vulnerable_to_data(\"$poke_2_type[0]\")");
             clearConnection($conn);
             $vulnerable_to2 = $conn->query("call vulnerable_to_data(\"$poke_2_type[1]\")");
             clearConnection($conn);

             if($vulnerable_to1){

                    echo "<div class='vulnerable'>Vulnerable Against: ";

                    $vulnerable = $vulnerable_to1->fetch_row();
                    $result_array = arrayLoop($vulnerable, $type_order, $result_array);
             }

             if($vulnerable_to2){

                   $vulnerable = $vulnerable_to2->fetch_row();
                   $result_array = arrayLoop($vulnerable, $type_order, $result_array);
            }

            //Array unique code taken from http://php.net/manual/en/function.array-unique.php
            $print = array_unique($result_array);
            printArray($print);
            echo "</div>";

            unset($result_array);
            

        }

     ?>



</body>
</html>
