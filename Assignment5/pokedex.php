<!--/*
	Assignment#5 CPSC2030
	Title: pokedex.php
	Author: Shea Melenka
	Date: October 21st 2018
*/-->
<!--CODE BUILT FROM PAGE1.PHP EXAMPLE-->
<!DOCTYPE html>
<html lang="en">
     <head>
         <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <meta http-equiv="X-UA-Compatible" content="ie=edge">
         <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less"/>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
         <title>Pokedex</title>
     </head>
     <?php
          //Setup
          include "sqlhelper.php";
          //include "stored_procedures.sql";
          //database stuff first
          //SQL  portion
          $user = 'CPSC2030';
          $pwd = 'CPSC2030';
          $server = 'localhost';
          $dbname = 'pokedex';

          $conn = new mysqli($server, $user, $pwd, $dbname);

     ?>

     <body>
          <a href=pokedex.php><h2>Home</h2></a>

          <h1>Choose a Pokemon:</h1>

          <?php
               //For reloading the page
               if (!empty($_GET["type"])) {

                   $type = mysqli_real_escape_string($conn, $_GET["type"]);

                   $dex_call = $conn->query("call pokedex_type(\"$type\")");
                   clearConnection($conn);

             }else{
                  //If it is the first time that the page is loaded
                   $dex_call = $conn->query("call pokedex_cards()");
                   clearConnection($conn);
              }

              if($dex_call){

                   $dex = $dex_call->fetch_all(MYSQLI_ASSOC);

                   echo "<div class='container'>";

                   //Sets up the cards
                   foreach($dex as $card){
                        echo "<div class='card'>";
                        echo $card["ID"]." ".$card["Name"];

                        $result1 = $conn->query("call single_type(\"$card[Name]\")");
                        clearConnection($conn);
                        $poke_1_type = $result1->fetch_row();

                        //type set up pokemon with one type
                        if (!empty($poke_1_type)) {

                              $link = "pokedex.php?type=".urlencode($poke_1_type[0]); //use urlencode to change special characters to the right format
                              echo "  <a href= '$link'>$poke_1_type[0]</a>";
                        }

                        $result2 = $conn->query("call multi_type(\"$card[Name]\")");
                        clearConnection($conn);
                        $poke_2_type = $result2->fetch_row();

                        //type set up for pokemon with two types
                       if (!empty($poke_2_type)) {

                            $link1 = "pokedex.php?type=".urlencode($poke_2_type[0]);
                            $link2 = "pokedex.php?type=".urlencode($poke_2_type[1]);

                            echo " <a href= '$link1'>$poke_2_type[0]</a> & <a href= '$link2'>$poke_2_type[1]</a>";
                       }

                       $link = "pokemon.php?name=".urlencode($card["Name"]); //use urlencode to change special characters to the right format
                       echo " "."<a href= '$link' class= 'details'>Detailed Stats</a>";  // Manually generate the link tags
                       echo "</div>";
                 }
                 echo "</div>";

             } else {
               echo "Error";
             }

          ?>

     </body>

</html>
