let character0 = {
     name: "Riley Miller",
     side: "Edinburgh Army",
     unit: "Federate Joint Ops",
     rank: "Second Lieutenant",
     role: "Artillery Advisor",
     description: "Born in the Galian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.",
     image: "http://valkyria.sega.com/img/character/chara02.png"
};

let character1 = {
     name: "Kai Schulen",
     side: "Edinburgh Army",
     unit: "Ranger Corps, Squad E",
     rank: "Sergeant Major",
     role: "Fireteam Leader",
     description: "Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename \"Deadeye Kai.\" Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.",
     image: "http://valkyria.sega.com/img/character/chara04.png"
};

let character2 = {
     name: "Minerva Victor",
     side: "Edinburgh Army",
     unit: "Ranger Corps, Squad F",
     rank: "First Lieutenant",
     role: "Senior Commander",
     description: "Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.",
     image: "http://valkyria.sega.com/img/character/chara11.png"
};

let character3 = {
     name: "Karen Stuart",
     side: "Edinburgh Army",
     unit: "Squad E",
     rank: "Corporal",
     role: "Combat EMT",
     description: "Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.",
     image: "http://valkyria.sega.com/img/character/chara12.png"
};

let character4 = {
     name: "Ragnarok",
     side: "Edinburgh Army",
     unit: "Squad E",
     rank: "K-9 Unit",
     role: "Mascot",
     description: "Once a stray, this good good boy is lovingly referred to as \"Rags.\"As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.",
     image: "http://valkyria.sega.com/img/character/chara13.png"
};

let character5 = {
     name: "Miles Arbeck",
     side: "Edinburgh Army",
     unit: "Ranger Corps, Squad E",
     rank: "Sergeant",
     role: "Tank Operator",
     description: "Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.",
     image: "http://valkyria.sega.com/img/character/chara15.png"
};

let character6 = {
     name: "Roland Morgen",
     side: "Edinburgh Navy",
     unit: "Centurion, Cygnus Fleet",
     rank: "Ship's Captain",
     role: "Cruiser Commander",
     description: "Born in the United Kingdom of Edinburgh, this naval officer commands a state-of-the-art snow cruiser named the Centurion. For a ship's captain, his disposition is surprisingly mild-mannered. As such, he never loses his composure, even in the direst of straits.",
     image: "http://valkyria.sega.com/img/character/chara18.png"
};

let character7 = {
     name: "Marie Bennett",
     side: "Edinburgh Navy",
     unit: "Centurion, Cygnus Fleet",
     rank: "Petty Officer",
     role: "Chief of Operations",
     description: "As the Centurion's crewmember responsible for overseeing daily operations, this gentle and supportive EWI veteran even takes daily tasks like cooking and cleaning upon herself. She never forgets to wear a smile. Her age is undisclosed, even in her personnel files.",
     image: "http://valkyria.sega.com/img/character/chara20.png"
};

let character8 = {
     name: "Louffe",
     side: "Edinburgh Navy",
     unit: "Centurion, Cygnus Fleet",
     rank: "Sergeant",
     role: "Radar Operator",
     description: "As the Centurion's crewmember responsible for route reconnaissance, this young Darcsen has a keen intellect and acerbic wit beyond her age. Although her harsh tongue keeps most people at arm's length, she's developed a strong companionship with Marie Bennet.",
     image: "http://valkyria.sega.com/img/character/chara21.png"
};

let character9 = {
     name: "André",
     side: "Edinburgh Navy",
     unit: "Centurion, Cygnus Fleet",
     rank: "Lieutenant",
     role: "Chief Engineer",
     description: "As the Centurion's crewmember responsible for maintenance and repairs, this EW1 veteran runs a tight ship. With a fondness for harsh language and hard liquor, André's overworked yet loyal mechanics fondly refer to him as the crew's grumpy old uncle.",
     image: "http://valkyria.sega.com/img/character/chara22.png"
};


let army = [character0, character1, character2, character3, character4, character5, character6, character7, character8, character9];
let squad = [];

function loadPage(){

     let cssClass = document.querySelectorAll(".roster");

     for(x=0; x<army.length; x++){
          cssClass[x].innerHTML = army[x].name;
     }
}

//Borrowed from examples
let rosterElements = document.querySelectorAll(".roster");
for(let element of rosterElements){

     element.onclick = function(event){
          for(let notElement of rosterElements){
               if (notElement.classList.contains("selected")){
                   notElement.classList.remove("selected");
               }
          }
          event.target.classList.add("selected");
          addToSquad(event.target);
     }

     element.onmouseover = function(event){

          for(x=0; x<army.length; x++){
               if(event.target.innerHTML == army[x].name){
                    displayProfile(army[x]);
               }
          }
     }
}

function addToSquad(element){

     if(squad.length < 5){
          let newMember = element.innerHTML;
          let repeatMember = false;
          for(let squadMember of squad){
               if(newMember == squadMember){
                    repeatMember = true;
               }
          }
          if(!repeatMember){
               squad[squad.length] = newMember;
          }
          clearDisplay();
          displaySquad();
     }
     consoleDisplay();//REMOVE
}

function displayProfile(character){
     let nameElement = document.querySelector(".name");
     nameElement.innerHTML = "<h4>Name: </h4>"+character.name;

     let sideElement = document.querySelector(".side");
     sideElement.innerHTML = "<h4>Side: </h4>"+character.side;

     let unitElement = document.querySelector(".unit");
     unitElement.innerHTML = "<h4>Unit: </h4>"+character.unit;

     let rankElement = document.querySelector(".rank");
     rankElement.innerHTML = "<h4>Rank: </h4>"+character.rank;

     let roleElement = document.querySelector(".role");
     roleElement.innerHTML = "<h4>Role: </h4>"+character.role;

     let descriptionElement = document.querySelector(".description");
     descriptionElement.innerHTML = "<h4>Description: </h4>"+character.description;

     let imageElement = document.querySelector(".image");
     imageElement.innerHTML = "<img class='characterImage' src='"+character.image+"' alt='Character Image'>";
}

//Borrowed from examples
let squadElements = document.querySelectorAll(".squad");
for(let element of squadElements){

     element.onclick = function(event){
          removeFromSquad(event.target);
     }

     element.onmouseover = function(event){

          for(x=0; x<army.length; x++){
               if(event.target.innerHTML == army[x].name){
                    displayProfile(army[x]);
               }
          }
     }
}

function removeFromSquad(element){

     let removeMember = element.innerHTML;
     element.innerHTML = null;
     let removeIndex = squad.indexOf(removeMember);
     squad.splice(removeIndex, 1);
     consoleDisplay();
}

function clearDisplay(){

     let squadClasses = document.querySelectorAll(".squad");

     for(let squadClass of squadClasses){
          squadClass.innerHTML = "";
     }
}

function displaySquad(){

     let squadClass = document.querySelectorAll(".squad");

     for(x=0; x<squad.length; x++){
          squadClass[x].innerHTML = squad[x];
     }
}

function hoverOver(){

}

function consoleDisplay(){
     console.log("squad[0] = "+squad[0]+", squad[1] = "+squad[1]+", squad[2] = "+squad[2]+", squad[3] = "+squad[3]+", squad[4] = "+squad[4]);
}
