<?php
     class character{
          public $name;
          public $side;
          public $unit;
          public $rank;
          public $role;
          public $description;
          public $image;

          function __construct($inName, $inSide, $inUnit, $inRank, $inRole, $inDescription, $inImage){
               $this->name = $inName;
               $this->side = $inSide;
               $this->unit = $inUnit;
               $this->rank = $inRank;
               $this->role = $inRole;
               $this->description = $inDescription;
               $this->image = $inImage;
          }
     }

     $character0 = new character("Riley Miller", "Edinburgh Army", "Federate Joint Ops", "Second Lieutenant", "Artillery Advisor", "Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.", "http://valkyria.sega.com/img/character/chara02.png");
     $character1 = new character("Kai Schulen", "Edinburgh Army", "Ranger Corps, Squad E", "Sergeant Major", "Fireteam Leader", "Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename \"Deadeye Kai.\" Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.", "http://valkyria.sega.com/img/character/chara04.png");
     $character2 = new character("Minerva Victor", "Edinburgh Army", "Ranger Corps, Squad F", "First Lieutenant", "Senior Commander", "Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.", "http://valkyria.sega.com/img/character/chara11.png");
     $character3 = new character("Karen Stuart", "Edinburgh Army", "Squad E", "Corporal", "Combat EMT", "Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.", "http://valkyria.sega.com/img/character/chara12.png");
     $character4 = new character("Ragnarok", "Edinburgh Army", "Squad E", "K-9 Unit", "Mascot", "Once a stray, this good good boy is lovingly referred to as \"Rags.\"As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.", "http://valkyria.sega.com/img/character/chara13.png");
     $character5 = new character("Miles Arbeck", "Edinburgh Army", "Ranger Corps, Squad E", "Sergeant", "Tank Operator", "Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.", "http://valkyria.sega.com/img/character/chara15.png");
     $character6 = new character("Roland Morgen", "Edinburgh Navy", "Centurion, Cygnus Fleet", "Ship's Captain", "Cruiser Commander", "Born in the United Kingdom of Edinburgh, this naval officer commands a state-of-the-art snow cruiser named the Centurion. For a ship's captain, his disposition is surprisingly mild-mannered. As such, he never loses his composure, even in the direst of straits.", "http://valkyria.sega.com/img/character/chara18.png");
     $character7 = new character("Marie Bennett", "Edinburgh Navy", "Centurion, Cygnus Fleet", "Petty Officer", "Chief of Operations", "As the Centurion's crewmember responsible for overseeing daily operations, this gentle and supportive EWI veteran even takes daily tasks like cooking and cleaning upon herself. She never forgets to wear a smile. Her age is undisclosed, even in her personnel files.", "http://valkyria.sega.com/img/character/chara20.png");
     $character8 = new character("Louffe", "Edinburgh Navy", "Centurion, Cygnus Fleet", "Sergeant", "Radar Operator", "As the Centurion's crewmember responsible for route reconnaissance, this young Darcsen has a keen intellect and acerbic wit beyond her age. Although her harsh tongue keeps most people at arm's length, she's developed a strong companionship with Marie Bennet.", "http://valkyria.sega.com/img/character/chara21.png");
     $character9 = new character("André", "Edinburgh Navy", "Centurion, Cygnus Fleet", "Lieutenant", "Chief Engineer", "As the Centurion's crewmember responsible for maintenance and repairs, this EW1 veteran runs a tight ship. With a fondness for harsh language and hard liquor, André's overworked yet loyal mechanics fondly refer to him as the crew's grumpy old uncle.", "http://valkyria.sega.com/img/character/chara22.png");











?>
