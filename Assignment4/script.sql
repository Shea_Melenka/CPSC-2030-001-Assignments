/*
	Assignment#4 CPSC2030
	Title: script.sql
	Author: Shea Melenka
	Date: October 15th 2018
*/

/*The Bottom 10 Pokémon ranked by speed*/
/*I understand this to mean the 10 slowest pokemon (smallest speed value)*/
SELECT *
FROM `pokemon`
ORDER BY Speed
LIMIT 10;

/*All Pokémon who is vulnerable to Ground and Resistant to Steel*/
/*I am assuming that duel type pokemon are effected in the same way, therefore they are included in the output*/
CREATE TABLE `table1`
AS SELECT Type
FROM `vulnerable_to`
WHERE Ground = 1;

CREATE TABLE `table2`
AS SELECT Type
FROM `resistant_to`
WHERE Steel = 1;

CREATE TABLE `table3`
AS SELECT `table1`.Type
FROM `table1`
INNER JOIN `table2`
ON `table1`.Type = `table2`.Type;

DROP TABLE `table1`;
DROP TABLE `table2`;

CREATE TABLE `output`
AS SELECT `pokemon`.*
FROM `pokemon`
INNER JOIN `table3`
ON `pokemon`.Type = `table3`.Type;

CREATE TABLE `name_table`
AS SELECT DISTINCT Name
FROM `two_types`
INNER JOIN `table3`
WHERE `two_types`.Type1 = `table3`.Type
OR `two_types`.Type2 = `table3`.Type;

DROP TABLE `table3`;

INSERT INTO `output`
SELECT `pokemon`.*
FROM `pokemon`
INNER JOIN `name_table`
ON `pokemon`.Name = `name_table`.Name;

DROP TABLE `name_table`;

SELECT * FROM `output`
ORDER BY ID;

DROP TABLE `output`;/*MAY NEED TO TAKE THIS OUT TO SEE ALL OF THE DATA IN A TABLE*/

/*All Pokémon who have a BST between 200 to 500 who is weak against water types*/
/*I am assuming that duel type pokemon are effected in the same way, therefore they are included in the output*/
/*I understand 'between' to not include the lower and upper bounds of 200 and 500*/
CREATE TABLE `table1`
AS SELECT Type
FROM `weak_against`
WHERE Water = 1;

CREATE TABLE `table2`
AS SELECT `pokemon`.*
FROM `pokemon`
INNER JOIN `table1`
ON `pokemon`.Type = `table1`.Type;

CREATE TABLE `name_table`
AS SELECT DISTINCT Name
FROM `two_types`
INNER JOIN `table1`
WHERE `two_types`.Type1 = `table1`.Type
OR `two_types`.Type2 = `table1`.Type;

DROP TABLE `table1`;

INSERT INTO `table2`
SELECT `pokemon`.*
FROM `pokemon`
INNER JOIN `name_table`
ON `pokemon`.Name = `name_table`.Name;

DROP TABLE `name_table`;

CREATE TABLE `output`
AS SELECT *
FROM `table2`
WHERE TotalStats > 200 AND TotalStats < 500;

DROP TABLE `table2`;

SELECT * FROM `output`
ORDER BY ID;

DROP TABLE `output`;/*MAY NEED TO TAKE THIS OUT TO SEE ALL OF THE DATA IN A TABLE*/

/*The Pokémon with the highest Atk, has a Mega evolution form and vulnerable to fire*/
/*I understand this to be asking for the one pokemon returned not to be a Mega evolution itself but to have a mega evolution avaliable*/
CREATE TABLE `table1`
AS SELECT Type
FROM `vulnerable_to`
WHERE Fire = 1;

CREATE TABLE `table2`
AS SELECT `pokemon`.*
FROM `pokemon`
INNER JOIN `table1`
ON `pokemon`.Type = `table1`.Type;

CREATE TABLE `name_table`
AS SELECT DISTINCT Name
FROM `two_types`
INNER JOIN `table1`
WHERE `two_types`.Type1 = `table1`.Type
OR `two_types`.Type2 = `table1`.Type;

DROP TABLE `table1`;

INSERT INTO `table2`
SELECT `pokemon`.*
FROM `pokemon`
INNER JOIN `name_table`
ON `pokemon`.Name = `name_table`.Name;

DROP TABLE `name_table`;

CREATE TABLE `table3`
AS SELECT `table2`.*
FROM `table2`
INNER JOIN `mega_evolutions`
ON `table2`.ID = `mega_evolutions`.ID;

DROP TABLE `table2`;

CREATE TABLE `output`
AS SELECT `table3`.*
FROM `table3`
Left JOIN `mega_evolutions`
ON `table3`.Name = `mega_evolutions`.Name
WHERE `mega_evolutions`.Name IS NULL;

DROP TABLE `table3`;

SELECT *
FROM `output`
ORDER BY Attack
DESC
LIMIT 1;

DROP TABLE `output`;/*MAY NEED TO TAKE THIS OUT TO SEE ALL OF THE DATA IN A TABLE*/
