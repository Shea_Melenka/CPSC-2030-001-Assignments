
let menuExpander = $(".expandableMenu");
let menuTitle = $(".menuTitle");

menuTitle.click(function(){

     if(menuExpander.hasClass("expanded")){
          menuExpander.removeClass("expanded");
          menuExpander.addClass("collapsed");
     }else{
          menuExpander.removeClass("collapsed");
          menuExpander.addClass("expanded");
     }

});

//DELAYED LINKING BORROWED FROM https://forum.webflow.com/t/delay-page-transition-for-animation-to-complete/52793

let item1 = $(".item1");
item1.click(function(event){
     item1.addClass("shoot1");
     event.preventDefault();
     let item1Link = $(".item1Link");
     let linkUrl = item1Link.attr("href");
     setTimeout(function(url) {window.location = url;}, 1000, linkUrl);
});

let item2 = $(".item2");
item2.click(function(event){
     item2.addClass("shoot2");
     event.preventDefault();
     let item2Link = $(".item2Link");
     let linkUrl = item2Link.attr("href");
     setTimeout(function(url) {window.location = url;}, 2000, linkUrl);
});

let item3 = $(".item3");
item3.click(function(event){
     item3.addClass("shoot3");
     event.preventDefault();
     let item3Link = $(".item3Link");
     let linkUrl = item3Link.attr("href");
     setTimeout(function(url) {window.location = url;}, 3000, linkUrl);
});

let item4 = $(".item4");
item4.click(function(event){
     item4.addClass("shoot4");
     event.preventDefault();
     let item4Link = $(".item4Link");
     let linkUrl = item4Link.attr("href");
     setTimeout(function(url) {window.location = url;}, 4000, linkUrl);
});

let item5 = $(".item5");
item5.click(function(event){
     item5.addClass("shoot5");
     event.preventDefault();
     let item5Link = $(".item5Link");
     let linkUrl = item5Link.attr("href");
     setTimeout(function(url) {window.location = url;}, 5000, linkUrl);
});
