-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2018 at 07:11 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `green_house`
   CREATE DATABASE `green_house`; 
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `flower_table` ()  BEGIN

     SELECT Name, Image, Info
     FROM flowers;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `submit_application` (IN `name` TEXT, IN `role` TEXT, IN `message` TEXT)  BEGIN

     INSERT INTO applications (Name, Role, Message)
     VALUES (name, role, message);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `tree_table` ()  BEGIN

     SELECT Name, Image, Info
     FROM trees;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `vegetable_table` ()  BEGIN

     SELECT Name, Image, Info
     FROM vegetables;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verify_user` (IN `username` TEXT, IN `password` TEXT)  BEGIN

     SELECT *
     FROM users
     WHERE username = users.Username
     AND password = users.Password;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `Name` varchar(255) DEFAULT NULL,
  `Role` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `flowers`
--

CREATE TABLE `flowers` (
  `Name` varchar(22) DEFAULT NULL,
  `Image` varchar(224) DEFAULT NULL,
  `Info` varchar(316) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `flowers`
--

INSERT INTO `flowers` (`Name`, `Image`, `Info`) VALUES
('Achillea millefolium', 'http://upload.wikimedia.org/wikipedia/commons/2/20/Achillea_millefolium_01.JPG', 'Achillea millefolium, commonly known as yarrow or common yarrow, is a flowering plant in the family Asteraceae. It is native to temperate regions of the Northern Hemisphere in Asia, Europe, and North America.'),
('Adenium obesum', 'http://upload.wikimedia.org/wikipedia/commons/9/91/Adenium.JPG', 'The desert rose (Adenium obesum) is a striking plant with succulent stems and deep red flowers. The plant is deciduous in cooler winters, but it can be kept in leaf provided there is sufficient warmth and light water.'),
('Allium bisceptrum', 'http://upload.wikimedia.org/wikipedia/commons/0/0d/Allium_Bisceptrum.jpg', 'Allium bisceptrum, also known as the twincrest onion, is a high elevation plant native to western United States. It is a perennial that thrives under damp and shady conditions or open meadows in California, Arizona, New Mexico, Nevada, Oregon, Washington, Idaho, and Utah.'),
('Alstroemeria', 'http://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Alstroemeria_aurantiaca.jpg/128px-Alstroemeria_aurantiaca.jpg', 'Alstroemeria, commonly called the Peruvian lily or lily of the Incas, is a genus of flowering plants in the family Alstroemeriaceae. They are all native to South America although some have become naturalized in the United States, Mexico, Australia, New Zealand, Madeira and the Canary Islands.'),
('Amaryllis Belladonna', 'http://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Amaryllis_belladonna_03.jpg/128px-Amaryllis_belladonna_03.jpg', 'Amaryllis belladonna, is a plant species native to Cape Province in South Africa but widely cultivated as an ornamental.'),
('Aquilegia', 'http://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aquilegia_bertolonii_Schott.JPG/128px-Aquilegia_bertolonii_Schott.JPG', 'Aquilegia is a genus of about 60–70 species of perennial plants that are found in meadows, woodlands, and at higher altitudes throughout the Northern Hemisphere, known for the spurred petals of their flowers.'),
('Aster', 'http://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Aster_amellus_sl_1.jpg/128px-Aster_amellus_sl_1.jpg', 'Aster is a genus of perennial flowering plants in the family Asteraceae. Its circumscription has been narrowed, and it now encompasses around 180 species, all but one of which are restricted to Eurasia; many species formerly in Aster are now in other genera of the tribe Astereae.'),
('Bellis perennis', 'http://upload.wikimedia.org/wikipedia/commons/thumb/0/06/RotesGaensebluemchen.jpg/128px-RotesGaensebluemchen.jpg', 'Bellis perennis is a common European species of daisy, of the Asteraceae family, often considered the archetypal species of that name. Many related plants also share the name \"daisy\", so to distinguish this species from other daisies it is sometimes qualified as common daisy, lawn daisy or English daisy.'),
('Centaurea Cyanus', 'http://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Bachelor%27s_button%2C_Basket_flower%2C_Boutonniere_flower%2C_Cornflower_-_3.jpg/128px-Bachelor%27s_button%2C_Basket_flower%2C_Boutonniere_flower%2C_Cornflower_-_3.jpg', 'Centaurea cyanus, commonly known as cornflower or bachelor\'s button, is an annual flowering plant in the family Asteraceae, native to Europe. In the past it often grew as a weed in cornfields, hence its name.'),
('Chrysanthemum', 'http://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Chrysanthemum_x_morifolium_Dompierre_2.jpg/128px-Chrysanthemum_x_morifolium_Dompierre_2.jpg', 'Chrysanthemums, sometimes called mums or chrysanths, are flowering plants of the genus Chrysanthemum in the family Asteraceae. They are native to Asia and northeastern Europe. Most species originate from East Asia and the center of diversity is in China. Countless horticultural varieties and cultivars exist.'),
('Crocosmia lucifer', 'http://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Crocosmia_lucifer.jpg/128px-Crocosmia_lucifer.jpg', 'Commonly known as Montbretia, Crocosmia. Regarded as one of the hardiest cultivars, \'Lucifer\' adds a tropical flair to northern gardens. Beginning in midsummer and continuing into early fall in some climates, brilliant flame red flowers stand in rows on wiry, gracefully arched stems that are perfect for cutting.'),
('Dahlia', 'http://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Kwiat_Dalii.JPG/128px-Kwiat_Dalii.JPG', 'Dahlia is a genus of bushy, tuberous, herbaceous perennial plants native to Mexico. A member of the Asteraceae, dicotyledonous plants, related species include the sunflower, daisy, chrysanthemum, and zinnia. There are 42 species of dahlia, with hybrids commonly grown as garden plants.'),
('Dietes grandiflora', 'http://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Dietes_grandiflora_or_Fairy_Iris.jpg/128px-Dietes_grandiflora_or_Fairy_Iris.jpg', 'Dietes grandiflora is a rhizomatous perennial plant with long, rigid, sword-like green leaves belonging to the Iridaceae family. This species is common in horticulture in its native South Africa, where it is often used in public gardens, beautification of commercial premises and along roadsides.'),
('Gardenia', 'http://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Gardenia_jasminoides_Double-flowered_s1.JPG/128px-Gardenia_jasminoides_Double-flowered_s1.JPG', 'Gardenia is a genus of flowering plants in the coffee family, Rubiaceae, native to the tropical and subtropical regions of Africa, Asia, Madagascar and Pacific Islands. The genus was named by Carl Linnaeus and John Ellis after Dr. Alexander Garden, a Scottish-born American naturalist.'),
('Gentiana dinarica', 'http://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Gentiana_dinarica.jpg/128px-Gentiana_dinarica.jpg', 'From the acaulis group of gentians, a superb mat forming perennial with impressive indigo trumpets. Grow in well drained conditions in a raised bed or in a large trough.'),
('Geranium', 'http://upload.wikimedia.org/wikipedia/commons/thumb/2/20/GERANIUM_SANGUINEUM_-_AGUDA_-_IB-725_%28Gerani_sanguini%29.JPG/128px-GERANIUM_SANGUINEUM_-_AGUDA_-_IB-725_%28Gerani_sanguini%29.JPG', 'Geranium is a genus of 422 species of flowering annual, biennial, and perennial plants that are commonly known as the cranesbills. They are found throughout the temperate regions of the world and the mountains of the tropics, but mostly in the eastern part of the Mediterranean region.'),
('Honeysuckle', 'http://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Bedarieux_fg12.jpg/512px-Bedarieux_fg12.jpg', 'Honeysuckles are arching shrubs or twining vines in the family Caprifoliaceae, native to the Northern Hemisphere. Approximately 180 species of honeysuckle have been identified.'),
('Hyacinthus', 'http://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/White_and_purple_hyacinths.JPG/128px-White_and_purple_hyacinths.JPG', 'Hyacinthus is a small genus of bulbous, fragrant flowering plants in the family Asparagaceae, subfamily Scilloideae. These are commonly called hyacinths. The genus is native to the eastern Mediterranean.'),
('Ipomea', 'http://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Flora_Oued_Zitoun6.JPG/128px-Flora_Oued_Zitoun6.JPG', 'Ipomoea is the largest genus in the flowering plant family Convolvulaceae, with over 500 species. It is a large and diverse group with common names including morning glory, water convolvulus or kangkung, sweet potato, bindweed, moonflower, etc.'),
('Lamiaceae', 'http://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Rotheca_myricoides_Blue_Butterfly_Bush.jpg/128px-Rotheca_myricoides_Blue_Butterfly_Bush.jpg', 'Lamiaceae (previously Verbenaceae) From spring to fall the Blue Butterfly Bush produces large clusters of dainty blue flowers that very much resemble little butterflies and thus the name. The Blue Butterfly Bush as we all lovingly have known it for many years is actually the cultivar \'Ugandense\'.'),
('Muscari', 'http://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Flower%2C_Grape_hyacinth_-_Flickr_-_nekonomania.jpg/256px-Flower%2C_Grape_hyacinth_-_Flickr_-_nekonomania.jpg', 'Muscari is a genus of perennial bulbous plants native to Eurasia that produce spikes of dense, most commonly blue, urn-shaped flowers resembling bunches of grapes in the spring. The common name for the genus is grape hyacinth, but they should not be confused with hyacinths.'),
('Myosotis', 'http://www.all-my-favourite-flower-names.com/images/Wald_Vergissmeinnicht-forget-me-nots.jpg', 'Myosotis is a genus of flowering plants in the family Boraginaceae. In the northern hemisphere they are colloquially denominated forget-me-nots or Scorpion grasses. The colloquial name \"Forget-me-not\" was calqued from the German Vergissmeinnicht and first used in English in AD 1398 through King Henry IV of England.'),
('Primula vulgaris', 'http://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Primrose_%283356633595%29.jpg/128px-Primrose_%283356633595%29.jpg', 'Primula vulgaris, the common primrose is a species of flowering plant in the family Primulaceae, native to western and southern Europe, northwest Africa, and parts of southwest Asia.'),
('Tagetes erecta', 'http://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Tagetes_erecta_02.JPG/128px-Tagetes_erecta_02.JPG', 'Tagetes erecta, the Mexican marigold or Aztec marigold, is a species of the genus Tagetes native to Mexico. Despite its being native to the Americas, it is often called African marigold. In Mexico, this plant is found in the wild in the states of México, Puebla, and Veracruz.'),
('Xerochrysum bracteatum', 'http://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Bracteantha_bracteata_%27Flobrafla%27_Flower_Closeup.JPG/128px-Bracteantha_bracteata_%27Flobrafla%27_Flower_Closeup.JPG', 'Xerochrysum bracteatum, commonly known as the golden everlasting or strawflower, is a flowering plant in the family Asteraceae native to Australia. Described by Étienne Pierre Ventenat in 1803, it was known as Helichrysum bracteatum for many years before being transferred to a new genus Xerochrysum in 1990.');

-- --------------------------------------------------------

--
-- Table structure for table `trees`
--

CREATE TABLE `trees` (
  `Name` varchar(11) DEFAULT NULL,
  `Image` varchar(203) DEFAULT NULL,
  `Info` varchar(666) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trees`
--

INSERT INTO `trees` (`Name`, `Image`, `Info`) VALUES
('Almond', 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Ametllesjuliol.jpg/220px-Ametllesjuliol.jpg', 'The almond is a species of tree native to Mediterranean climate regions of the Middle East, from Syria and Turkey to India and Pakistan, although it has been introduced elsewhere.'),
('Apple', 'https://www.starkbros.com/images/dynamic/2979.jpg', 'An apple is a sweet, edible fruit produced by an apple tree(Malus pumila). Apple trees are cultivated worldwide, and are the most widely grown species in the genus Malus. The tree originated in Central Asia, where its wild ancestor, Malus sieversii, is still found today. Apples have been grown for thousands of years in Asia and Europe, and were brought to North America by European colonists. Apples have religious and mythological significance in many cultures, including Norse, Greek and European Christian traditions.'),
('Apricot', 'http://mcm-bt0hsn6c.stackpathdns.com/wp-content/uploads/2016/02/apricot.jpg', 'An apricot is a fruit, or the tree that bears the fruit, of several species in the genus Prunus (stone fruits). Usually, an apricot tree is from the species P. armeniaca, but the species P. brigantina, P. mandshurica, P. mume, P. zhengheensis and P. sibirica are closely related, have similar fruit, and are also called apricots.'),
('Avocado', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Persea_americana_fruit_2.JPG/220px-Persea_americana_fruit_2.JPG', 'The avocado (Persea americana) is a tree, long thought to have originated in South Central Mexico, classified as a member of the flowering plant family Lauraceae. The fruit of the plant, also called an avocado (or avocado pear or alligator pear), is botanically a large berry containing a single large seed known as a \"pit\" or a \"stone\".'),
('Cashew', 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Cashew_apples.jpg/220px-Cashew_apples.jpg', 'The cashew tree (Anacardium occidentale) is a tropical evergreen tree that produces the cashew seed (nut) and the cashew apple. It can grow as high as 14 m (46 ft), but the dwarf cashew, growing up to 6 m (20 ft), has proved more profitable, with earlier maturity and higher yields.'),
('Cherry', 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Black_Che.jpg/220px-Black_Che.jpg', 'A cherry is the fruit of many plants of the genus Prunus, and is a fleshy drupe (stone fruit). The cherry fruits of commerce usually are obtained from cultivars of a limited number of species such as the sweet cherry (Prunus avium) and the sour cherry (Prunus cerasus). The name \'cherry\' also refers to the cherry tree and its wood, and is sometimes applied to almonds and visually similar flowering trees in the genus Prunus, as in \"ornamental cherry\" or \"cherry blossom\".'),
('Damson', 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Damson_plum_fruit.jpg/220px-Damson_plum_fruit.jpg', 'The damson or damson plum (Prunus domestica or Prunus insititia), also archaically called the \"damascene\" is an edible drupaceous fruit, a subspecies of the plum tree. Varieties of insititia are found across Europe, but the name \"damson\" is derived from and most commonly applied to forms which are native to Great Britain. Damsons are relatively small plum-like fruit with a distinctive, somewhat astringent taste, and are widely used for culinary purposes, particularly in fruit preserves or jam.'),
('Elderberry', 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Sambucus-berries.jpg/220px-Sambucus-berries.jpg', 'Sambucus is a genus of flowering plants in the familyAdoxaceae. The various species are commonly called elder or elderberry. The genus was formerly placed in the honeysucklefamily, Caprifoliaceae, but was reclassified as Adoxaceae due to genetic and morphological comparisons to plants in the genus Adoxa.'),
('Grapefruit', 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Grapefruit.ebola.jpeg/170px-Grapefruit.ebola.jpeg', 'The grapefruit (Citrus × paradisi) is a subtropical citrus tree known for its sour to semi-sweet, somewhat bitter fruit. Grapefruit is a hybrid originating in Barbados as an accidental cross between two introduced species, sweet orange (C. sinensis) and pomelo or shaddock (C. maxima), both of which were introduced from Asia in the seventeenth century. When found, it was named the \"forbidden fruit\"; and frequently, it has been misidentified with the pomelo.'),
('Guava', 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Goya_blancs.JPG/220px-Goya_blancs.JPG', 'Guava is a common tropical fruit cultivated and enjoyed in many tropical and subtropical regions. Psidium guajava (common guava, lemon guava) is a small tree in the myrtle family (Myrtaceae), native to Mexico, Central America, and northern South America. Although related species may also be called guavas, they belong to other species or genera, such as the \"pineapple guava\" Acca sellowiana. In 2011, India was the largest producer of guavas.'),
('Lemon', 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/P1030323.JPG/220px-P1030323.JPG', 'The lemon, Citrus limon (L.) Osbeck, is a species of small evergreen tree in the flowering plant family Rutaceae, native to South Asia, primarily North eastern India. The tree\'s ellipsoidal yellow fruit is used for culinary and non-culinary purposes throughout the world, primarily for its juice, which has both culinary and cleaning uses. The pulp and rind (zest) are also used in cooking and baking. The juice of the lemon is about 5% to 6% citric acid, with a pH of around 2.2, giving it a sour taste. The distinctive sour taste of lemon juice makes it a key ingredient in drinks and foods such as lemonade and lemon meringue pie.'),
('Lime', 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Lime_Blossom.jpg/220px-Lime_Blossom.jpg', 'A lime is a hybrid citrus fruit, which is typically round, green in color, 3–6 centimetres (1.2–2.4 in) in diameter, and contains acidic juice vesicles. There are several species of citrus trees whose fruits are called limes, including the Key lime (Citrus aurantifolia), Persian lime, kaffir lime, and desert lime. Limes are a rich source of vitamin C, sour and are often used to accent the flavours of foods and beverages. They are grown year-round. Plants with fruit called \"limes\" have diverse genetic origins; limes do not form a monophyletic group.'),
('Loquat', 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Loquat-0.jpg/220px-Loquat-0.jpg', 'The loquat (Eriobotrya japonica) is a species of flowering plant in the family Rosaceae, a native to the cooler hill regions of China to south-central China. It is also quite common in Japan, Korea, hilly Regions of India (Himachal), Potohar and foothill regions of Pakistan and some can be found in some Northern part of the Philippines, and hill country in Sri Lanka. It can also be found in some southern European countries such as Cyprus, Malta, Italy, Spain and Portugal; and several Middle Eastern countries like Israel, Lebanon and Turkey.'),
('Lychee', 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Litchi_chinensis_fruits.JPG/220px-Litchi_chinensis_fruits.JPG', 'Lychee is the sole member of the genus Litchi in the soapberry family, Sapindaceae. A tall evergreen tree, the lychee bears small fleshy fruits. The outside of the fruit is pink-red, roughly textured and inedible, covering sweet flesh eaten in many different dessert dishes. Since the perfume-like flavour is lost in the process of canning, the fruit is usually eaten fresh.'),
('Mango', 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Mangoes_pic.jpg/220px-Mangoes_pic.jpg', 'Mangoes are juicy stone fruit (drupe) from numerous species of tropical trees belonging to the flowering plant genus Mangifera, cultivated mostly for their edible fruit. The majority of these species are found in nature as wild mangoes. The genus belongs to the cashew family Anacardiaceae. Mangoes are native to South Asia, from where the \"common mango\" or \"Indian mango\", Mangifera indica, has been distributed worldwide to become one of the most widely cultivated fruits in the tropics. Other Mangifera species (e.g. horse mango, Mangifera foetida) are grown on a more localized basis.'),
('Mulberry', 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Morus_alba_FrJPG.jpg/220px-Morus_alba_FrJPG.jpg', 'Morus, a genus of flowering plants in the family Moraceae, comprises 10–16 species of deciduous trees commonly known as mulberries, growing wild and under cultivation in many temperate world regions. Mulberries are fast-growing when young, but soon become slow-growing and rarely exceed 10–15 metres (30–50 ft) tall. The leaves are alternately arranged, simple and often lobed and serrated on the margin. Lobes are more common on juvenile shoots than on mature trees. The trees can be monoecious or dioecious.'),
('Olive', 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Olivesfromjordan.jpg/220px-Olivesfromjordan.jpg', 'The olive, known by the botanical name Olea europaea, meaning \"European olive\", is a species of small tree in the family Oleaceae, found in the Mediterranean Basin from Portugal to the Levant, the Arabian Peninsula, and southern Asia as far east as China, as well as the Canary Islands and Réunion. The species is cultivated in many places and considered naturalized in all the countries of the Mediterranean coast, as well as in Argentina, Saudi Arabia, Java, Norfolk Island, California, and Bermuda. Olea europaea is the type species for the genus Olea.'),
('Orange', 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/OrangeBloss_wb.jpg/220px-OrangeBloss_wb.jpg', 'The orange is the fruit of the citrus species Citrus × sinensis in the family Rutaceae. It is also called sweet orange, to distinguish it from the related Citrus × aurantium, referred to as bitter orange. The sweet orange reproduces asexually (apomixis through nucellar embryony); varieties of sweet orange arise through mutations. The orange is a hybrid between pomelo (Citrus maxima) and mandarin (Citrus reticulata). The chloroplast genome, and therefore the maternal line, is that of pomelo. The sweet orange has had its full genome sequenced.'),
('Peach', 'https://www.fast-growing-trees.com/images/P/Harvester-Peach-Tree-450w.jpg', 'The peach (Prunus persica) is a deciduous tree native to the region of Northwest China between the Tarim Basin and the north slopes of the Kunlun Mountains, where it was first domesticated and cultivated. It bears an edible juicy fruit called a peach or a nectarine. Peaches and nectarines are the same species, even though they are regarded commercially as different fruits. In contrast to peaches, whose fruits present the characteristic fuzz on the skin, nectarines are characterized by the absence of fruit-skin trichomes (fuzz-less fruit); it is thought that a mutation in a single gene (MYB25) is responsible for the hair or no-hair difference between the two.'),
('Pear', 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Pears.jpg/220px-Pears.jpg', 'The pear tree and shrub are a species of genus Pyrus, in the family Rosaceae, bearing the pomaceous fruit of the same name. Several species of pear are valued for their edible fruit and juices while others are cultivated as trees. The pear is native to coastal and mildly temperate regions of the Old World, from Western Europe and north Africa east right across Asia. It is a medium-sized tree, reaching 10–17 metres (33–56 ft) tall, often with a tall, narrow crown; a few species are shrubby.'),
('Pecan', 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Pecan-nuts-on-tree.jpg/120px-Pecan-nuts-on-tree.jpg', 'The pecan (Carya illinoinensis) is a species of hickory native to Mexico and the Southern United States. Pecans were one of the most recently domesticated major crops. Although wild pecans were well known among native and colonial Americans as a delicacy, the commercial growing of pecans in the United States did not begin until the 1880s.'),
('Plum', 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Closeup_of_blackthorn_aka_sloe_aka_prunus_spinosa_sweden_20050924.jpg/220px-Closeup_of_blackthorn_aka_sloe_aka_prunus_spinosa_sweden_20050924.jpg', 'A plum is a fruit of the subgenus Prunus of the genus Prunus.The subgenus is distinguished from other subgenera (peaches, cherries, bird cherries, etc.) in the shoots having terminal budand solitary side buds (not clustered), the flowers in groups of one to five together on short stems, and the fruit having a groove running down one side and a smooth stone (or pit).'),
('Pomelo', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Pomelo_In_Village.jpg/220px-Pomelo_In_Village.jpg', 'The pomelo, Citrus maxima or Citrus grandis, is the largest citrus fruit from the Rutaceae family. It is a natural (non-hybrid) citrus fruit, similar in appearance to a large grapefruit, native to South and Southeast Asia. The pomelo is one of the original citrus species from which the rest of cultivated citrus hybridized. The popular fruit is used in many Chinese festive celebrations throughout Southeast Asia.'),
('Pomegranate', 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Pomegranate_fruit.jpg/249px-Pomegranate_fruit.jpg', 'The pomegranate (Punica granatum) is a fruit-bearing deciduous shrub or small tree in the family Lythraceae that grows between 5 and 10 m (16 and 33 ft) tall. The pomegranate originated in the region extending from modern-day Iran to northern India, and has been cultivated since ancient times throughout the Mediterranean region. It was introduced into Spanish America in the late 16th century and into California by Spanish settlers in 1769.'),
('Walnut', 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Juglans_regia_2009_G2.jpg/220px-Juglans_regia_2009_G2.jpg', 'A walnut is the nut of any tree of the genus Juglans (Family Juglandaceae), particularly the Persian or English walnut, Juglans regia. Technically a walnut is the seed of a drupe or drupaceous nut and thus not a true botanical nut. It is used for food after being processed, while green for pickled walnuts or after full ripening for its nutmeat. Nutmeat of the eastern black walnut from the Juglans nigra is less commercially available, as are butternut nutmeats from Juglans cinerea. The walnut is nutrient-dense with protein and essential fatty acids.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Username` varchar(16) DEFAULT NULL,
  `Password` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Username`, `Password`) VALUES
('Rachel Porter', 'Birdcage'),
('Michael Jennings', 'Allcom'),
('Alex Danvers', 'DEOaccess'),
('Sam Arias', 'Ruby'),
('Lena Luthor', 'Reign'),
('Guest', 'Limited1xAccess');

-- --------------------------------------------------------

--
-- Table structure for table `vegetables`
--

CREATE TABLE `vegetables` (
  `Name` varchar(12) DEFAULT NULL,
  `Image` varchar(207) DEFAULT NULL,
  `Info` varchar(410) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vegetables`
--

INSERT INTO `vegetables` (`Name`, `Image`, `Info`) VALUES
('Asparagus', 'https://img.crocdn.co.uk/images/products2/pl/20/00/01/60/pl2000016092.jpg', 'Asparagus is a perennial vegetable, taking two or three years to become established, but it lives for 20 to 30 years. Asparagus requires minimal care and produces sweet, tender stalks every spring.'),
('Bean', 'https://www.almanac.com/sites/default/files/styles/primary_image_in_article/public/image_nodes/green-beans-1891801_1920.jpg?itok=LTZsOATe', 'Plant beans after the soil is warm, and don’t soak the seeds first, contrary to popular belief. They’ll rot or crack. Bush beans produce a lot of beans over a few weeks; the harvest season of pole beans extends until frost. Try shell beans as well.'),
('Broccoli', 'http://demandware.edgesuite.net/sits_pod32/dw/image/v2/BBBW_PRD/on/demandware.static/-/Sites-jss-master/default/dw1caf833f/images/products/vegetables/02812_01_blue_wind.jpg?sw=387&cx=302&cy=0&cw=1196&ch=1196', 'Broccoli is a cool-season crop that tolerates late spring frosts. Fresh broccoli tastes infinitely better than commercially-produced counterparts. Cut stalks off the main plant for a continual harvest. Rotate crops to minimize insect problems.'),
('Cabbage', 'https://edge.bonnieplants.com/www/uploads/20180920004236/stonehead-cabbage.jpg', 'Start cabbage early in the spring when the soil is soft or set out transplants. Like brussel sprouts, cabbage likes a nice, long growing season and plenty of moisture. Cabbage stores well for the winter.'),
('Carrot', 'https://pixfeeds.com/images/fruits-vegetables/carrots/1200-470332802-carrots-growing.jpg', 'Carrots are slow to germinate, especially in dry conditions, but they take up little room and are easy to grow. Thin carrots to 2 inches apart and grow half-length varieties, such as ‘Danvers Half-Long’ if you have heavy soil.'),
('Cauliflower', 'https://www.veseys.com/media/catalog/product/cache/image/700x700/e9c3970ab036de70892d86c6d221abfe/1/2/12890a-12890a-image-12890-steady.jpg', 'Cauliflower is a bit trickier to grow than some of the other brassicas. It doesn’t tolerate heat or frost, but needs at least two to three months to mature. Cover the cauliflower head with the leaves of the plant when it is about the size of an egg to “blanch” the cauliflower, which turns it white and improves its taste.'),
('Celery', 'https://www.rocketgardens.co.uk/wp-content/uploads/2016/02/celery-800x602.jpg', 'Home gardeners seldom grow celery. It grows slowly, requiring as much as six months to mature, and prefers a cool, moist climate and very rich, wet soil. Read about celery health benefits.'),
('Eggplant', 'https://www.seedsavers.org/site/img/seo-images/0370C-florida-high-bush-eggplant.jpg', 'Those shiny, purple lobes are the royalty of any garden. Grow eggplant as you would tomatoes. Plant seedlings in a warm, sunny location after the last frost. Keep the soil evenly moist and protect eggplants from late spring frosts by using row covers or cloches.'),
('Garlic', 'http://www.eagriculture.in/wp-content/uploads/2015/11/Garlic.jpg', 'Plant garlic in early spring. Purchase varieties adapted for your climate zone and plant the cloves pointed ends up and 2 to 3 inches deep. Dig them up when the tops die back.'),
('Kale', 'https://edge.bonnieplants.com/www/uploads/20180920003748/Winterbor-Kale.jpg', 'Kale is a cool season vegetable that plants well from seed or a transplant. It is successful as a spring or fall crop. Harvest leaves before heat, since higher temperatures make the leaves bitter. Fertilize kale with a nitrogen rich fertilizer about a month after transplanting or when the plants are about 4 to 5 inches tall. Kale is rich in vitamin A and vitamin C, making it a great vegetable for nutrition.'),
('Leek', 'https://www.almanac.com/sites/default/files/styles/primary_image_in_article/public/images/onion_plant.jpg?itok=d2jIYlRp', 'These mild-flavored cousins to onions are expensive to buy at the grocery store, but easy to grow in the garden. Sow them from seed in early spring or use transplants. Dig them with a trowel rather than pulling them to harvest. Mulch them to overwinter because they don’t store well.'),
('Lettuce', 'https://www.ces.ncsu.edu/wp-content/uploads/2014/08/LettucePlants-300x180.jpg', 'Plant lettuce in early spring, as soon as the soil is soft. Choose soft-headed lettuces over head lettuce, which tend to go to seed more quickly. Keep the soil moist so the lettuce is tender and mild.'),
('Onion', 'https://www.pinoynegosyo.net/wp-content/uploads/2017/07/onion-planting-1024x628.jpg', 'Onions take a long time to grow from seed and are often grown from sets instead. Buy onion sets locally, choosing those adapted to your area.'),
('Parsnip', 'https://www.thespruce.com/thmb/BwYtRH6r5NLnuFubpcJ8VYR0Qw0=/3000x2000/filters:no_upscale():fill(transparent,1)/Parsnipplants-GettyImages-155284457-59d5af19519de20010fe1720.jpg', 'Parsnips look like white carrots, but have a texture closer to potatoes. Use these root vegetables in soups, stews or roasted. Sow parsnips in spring and keep the soil evenly moist. Like carrots, they are slow to germinate, and may take as long as a year to grow. They benefit from a few frosts.'),
('Pepper', 'https://www.lovethegarden.com/sites/default/files/styles/full_width_700/public/files/Group%20of%20different%20coloured%20bell%20pepper%20plants-LR.jpg?itok=fyNVFVAV', 'Sweet peppers need a warm location and moist soil to develop the thick-walled, sweet fruit. They are more difficult to grow in dry regions than chili peppers.'),
('Potato', 'https://redwheelbarrowplants.files.wordpress.com/2013/02/potatoes-dug-up2.jpg?w=768', 'This staple crop is drought-resistant and yields more protein per square foot than any other crop but legumes. Start with certified disease-free seed potatoes and rotate crops every year to minimize disease.'),
('Pumpkin', 'https://www.publicdomainpictures.net/pictures/110000/velka/pumpkin-patch-1421332072nJE.jpg', 'Pumpkins take up a lot of space in the garden with their long, lazy vines, but their cheerful, orange or white fruit are worth the space. Plant them in late spring when the soil is warm.'),
('Radish', 'https://www.wholefoodsmarket.com/sites/default/files/wp-content/uploads/2010/05/391155_1376_0.jpg', 'Radishes are a good crop for the beginning gardener or even a child. They germinate quickly, take up little space and reach maturity in four to six weeks. Use them in salads or eat fresh.'),
('Rhubarb', 'https://www.pennywoodward.com.au/wp-content/uploads/2011/12/Rhubarb-vigorously-growing-387x257.jpg', 'This old-fashioned perennial plant is actually a vegetable, but is eaten as a fruit. Give it a large space in full-sun and keep the soil moderately moist. Harvest rhubarb in mid spring to early summer, but don’t eat the leaves, which are toxic.'),
('Spinach', 'http://gardeningwithcharlie.com/wp-content/uploads/2012/01/DSCN7121-300x225.jpg', 'Delicious in salads or steamed, spinach is one of the earliest garden greens. It goes to seed as soon as temperatures spike. Try new smooth varieties that are easier to wash than the crinkled type.'),
('Sweet potato', 'https://www.thespruce.com/thmb/ZnDbiIF-yhrpQwZhPX9StzwcuRw=/960x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/Sweet-potatoes-GettyImages-592411298-58bc9a215f9b58af5cc1ae4f.jpg', 'Sweet potatoes need at least five months of warm weather, making them a popular crop for Southern gardeners. Try adapted varieties if you live in the North.'),
('Tomato', 'https://5.imimg.com/data5/RM/PM/GLADMIN-24293168/tomato-plants-500x500.png', 'Tomatoes are the most commonly grown garden vegetable, and with good reason. Home-grown tomatoes are infinitely better than those found in the grocery store. Plant them from seedlings after the last frost and choose disease-resistant varieties.'),
('Turnip', 'https://opimedia.azureedge.net/-/media/images/hgr/editorial/articles/magazine-articles/2017/spring/spring-companions-turnips-and-peas/turnips-jpg.jpg', 'The ultimate peasant food, turnips are a fast-growing root vegetable that thrives in cool weather. Boil them, mash them or roast them.'),
('Watermelon', 'https://5.imimg.com/data5/QH/WS/MY-29907888/watermelon-plants-500x500.png', 'Watermelon needs long, hot summers and plenty of water to mature. Grow adapted, short-season types if you live in the north.'),
('Zucchini', 'https://img-aws.ehowcdn.com/350x235p/photos.demandstudios.com/getty/article/244/234/200353175-001_XS.jpg', 'Zucchini is actually a specific type of summer squash, so it is grown using the same methods as summer squash. A great choice if you are looking to get a lot of vegetables from one plant, because they make a lot.');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
