
let imageSlideshow = ["images/insideGreenHouse.jpg", "images/slideshow1.jpg", "images/flowers.jpg", "images/grapeTrees.jpg",
"images/slideshow2.jpg", "images/appleTrees.jpg", "images/flowers2.jpg", "images/trees.jpg", "images/vegetables.jpg", "images/flowers3.jpg"];

let slideshowIndex = 0;

let slideshowTimer = setInterval(changePicture, 10000);

function changePicture(){
     slideshowIndex = (slideshowIndex+1)%10;

     let slideshowImageClass = $(".slideshowImage");
     slideshowImageClass.attr("src",""+imageSlideshow[slideshowIndex]+"");
}


let logonButton = $(".logonButton");

logonButton.click(function(){

     let username = $("input[name=username]").val();
     let password = $("input[name=password]").val();

     $.ajax({
        url: "userLogin.php",
        method: "POST",
        data: {
            username: username,
            password: password
        },
        success: function(msg){
           let data = JSON.parse(msg);

                if(data[0].userExists == true){

                     console.log("login success");

                     console.log("username: "+username);

                     $.ajax({
                        url: "secret/getUser.php",
                        method: "POST",
                        data: {
                            username: username
                        },
                        success: function(){
                             console.log("second ajax successful");
                              window.location.href = "http://localhost/project/secret/index.php?page=home";
                        }
                   })


                }else{
                     console.log("login failed");
                     window.location.href = "http://localhost/project/home.php";
                }



        }
    })

});
