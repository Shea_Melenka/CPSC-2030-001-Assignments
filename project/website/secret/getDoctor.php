
<?php

	require_once 'sqlhelper.php';
	require_once '../vendor/autoload.php';  //include the twig library.

	$twig = setupMyTwigEnvironment();

	$conn = connectToMyDatabase();

	if($conn){

		$doctorName = $_POST["doctorName"];
		$getDoctorResult = $conn->query("call get_specific_doctor(\"$doctorName\")");
          clearConnection($conn);

		$doctorTestsTable = $getDoctorResult->fetch_all(MYSQLI_ASSOC);

		$output = array();

		foreach ($doctorTestsTable as $doctorTest){

			array_push($output,array(
				"date"=> $doctorTest["Date"],
				"doctor"=> $doctorTest["Doctor"],
				"patient"=> $doctorTest["Patient"],
				"destructive"=> $doctorTest["Destructive_Technology"],
				"restorative"=> $doctorTest["Restorative_Technology"],
				"description"=> $doctorTest["Test_Description"],
				"results"=> $doctorTest["Results"]));
		}
		echo json_encode($output);

	}else {

		//One benefit is that we can load a full error page
		$template = $twig->load("error.twig.html");
		echo $template->render(array("message"=>"Title query failed"));
	}





?>
