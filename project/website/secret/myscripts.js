let userClass = $(".user");
let user = userClass.text();
user = $.trim(user);

$.ajax({
   url: "userRole.php",
   method: "POST",
   data: {
       username: user
   },
   success: function(msg){
        let data = JSON.parse(msg);

        if(data[0].userRole != "Guest"){

             userClass.text(""+data[0].userRole+" "+user);

        }

   }
})

let imageSlideshow = ["images/botany.jpg", "images/lab.jpg", "images/medicalRoom.jpg", "images/lab2.jpg",
"images/storage.jpg"];

let slideshowIndex = 0;

let slideshowTimer = setInterval(changePicture, 10000);

function changePicture(){
     slideshowIndex = (slideshowIndex+1)%5;

     let slideshowImageClass = $(".slideshowImage");
     slideshowImageClass.attr("src",""+imageSlideshow[slideshowIndex]+"");
}


function getSpecificDoctor(doctorName){

     $.ajax({
        url: "getDoctor.php",
        method: "POST",
        data: {
            doctorName: doctorName
        },
        success: function(msg){
             let data = JSON.parse(msg);

             testLogList.empty();

             for(i = 0; i < data.length; i++) {

                  testLogList.append("<div class='test'><div class='testInfo testDate'>"+data[i].date+"</div><div class='testInfo testDoctor'>Doctor: "+data[i].doctor+"</div><div class='testInfo testPatient'>Patient: "+data[i].patient+"</div><div class='testInfo testDestructive'>Destructive Technology: "+data[i].destructive+"</div><div class='testInfo testRestorative'>Restorative Technology: "+data[i].restorative+"</div><div class='testInfo testDescription'>Test Description: "+data[i].description+"</div><div class='testInfo testResults'>Results: "+data[i].results+"</div></div>");

             }

        }
    })

}

function getSpecificPatient(patientName){

     $.ajax({
        url: "getPatient.php",
        method: "POST",
        data: {
            patientName: patientName
        },
        success: function(msg){
             let data = JSON.parse(msg);

             testLogList.empty();

             for(i = 0; i < data.length; i++) {

                  testLogList.append("<div class='test'><div class='testInfo testDate'>"+data[i].date+"</div><div class='testInfo testDoctor'>Doctor: "+data[i].doctor+"</div><div class='testInfo testPatient'>Patient: "+data[i].patient+"</div><div class='testInfo testDestructive'>Destructive Technology: "+data[i].destructive+"</div><div class='testInfo testRestorative'>Restorative Technology: "+data[i].restorative+"</div><div class='testInfo testDescription'>Test Description: "+data[i].description+"</div><div class='testInfo testResults'>Results: "+data[i].results+"</div></div>");

             }

        }
    })

}

function getSpecificDestructive(destructiveTech){

     $.ajax({
        url: "getDestructive.php",
        method: "POST",
        data: {
            destructiveTech: destructiveTech
        },
        success: function(msg){
             let data = JSON.parse(msg);

             testLogList.empty();

             for(i = 0; i < data.length; i++) {

                  testLogList.append("<div class='test'><div class='testInfo testDate'>"+data[i].date+"</div><div class='testInfo testDoctor'>Doctor: "+data[i].doctor+"</div><div class='testInfo testPatient'>Patient: "+data[i].patient+"</div><div class='testInfo testDestructive'>Destructive Technology: "+data[i].destructive+"</div><div class='testInfo testRestorative'>Restorative Technology: "+data[i].restorative+"</div><div class='testInfo testDescription'>Test Description: "+data[i].description+"</div><div class='testInfo testResults'>Results: "+data[i].results+"</div></div>");

             }

        }
    })

}

function getSpecificRestorative(restorativeTech){

     $.ajax({
        url: "getRestorative.php",
        method: "POST",
        data: {
            restorativeTech: restorativeTech
        },
        success: function(msg){
             let data = JSON.parse(msg);

             testLogList.empty();

             for(i = 0; i < data.length; i++) {

                  testLogList.append("<div class='test'><div class='testInfo testDate'>"+data[i].date+"</div><div class='testInfo testDoctor'>Doctor: "+data[i].doctor+"</div><div class='testInfo testPatient'>Patient: "+data[i].patient+"</div><div class='testInfo testDestructive'>Destructive Technology: "+data[i].destructive+"</div><div class='testInfo testRestorative'>Restorative Technology: "+data[i].restorative+"</div><div class='testInfo testDescription'>Test Description: "+data[i].description+"</div><div class='testInfo testResults'>Results: "+data[i].results+"</div></div>");

             }

        }
    })

}


//Sidebar menu
let testLogList = $(".testLogList");

let menuItem = $(".menuItem");
menuItem.click(function(){
     menuItem.removeClass("selected");
     $(this).addClass("selected");
});

//doctors
let drNum = 3; //number of distinct doctors could be gotten from the server
for(i = 0; i < drNum; i++){
     let dr = $(".dr"+i);
     dr.click(function(){
          let doctorName = dr.text();
          getSpecificDoctor(doctorName);
     });
}

//patients
let ptNum = 2; //number of distinct patients could be gotten from the server
for(i = 0; i < ptNum; i++){
     let pt = $(".pt"+i);
     pt.click(function(){
          let patientName = pt.text();
          getSpecificPatient(patientName);
     });
}

//destructive technology
let dtNum = 3; //number of distinct destructive technology could be gotten from the server
for(i = 0; i < dtNum; i++){
     let dt = $(".dt"+i);
     dt.click(function(){
          let destructiveTech = dt.text();
          getSpecificDestructive(destructiveTech);
     });
}

//restorative technology
let rtNum = 24; //number of distinct restorative technology could be gotten from the server
for(i = 0; i < rtNum; i++){
     let rt = $(".rt"+i);
     rt.click(function(){
          let restorativeTech = rt.text();
          getSpecificRestorative(restorativeTech);
     });
}

let sendApplication = $(".sendApplication");
sendApplication.click(function(){

     let name = $("input[name=applicationName]").val();
     let role = $("input[name=role]").val();
     let message = $("textarea[name=applicationMessage]").val();

     $.ajax({
        url: "submitApplication.php",
        method: "POST",
        data: {
            name: name,
            role: role,
            message: message
        },
        success: function(){
             console.log("login success");

             let application = $(".application");
             let applicationTitle = $(".applicationTitle");
             application.addClass("successfulSubmission");
             applicationTitle.text("Thank You for Submitting Your Application");

        }
    })

});

let sendFeedback = $(".sendFeedback");
sendFeedback.click(function(){

     let name = $("input[name=feedbackName]").val();
     let qualifications = $("input[name=qualifications]").val();
     let message = $("textarea[name=feedbackMessage]").val();

     $.ajax({
        url: "submitFeedback.php",
        method: "POST",
        data: {
            name: name,
            qualifications: qualifications,
            message: message
        },
        success: function(){
             console.log("login success");

             let feedback = $(".feedback");
             let feedbackTitle = $(".feedbackTitle");
             feedback.addClass("successfulSubmission");
             feedbackTitle.text("Thank You for Sending Us Feedback");

        }
    })

});
