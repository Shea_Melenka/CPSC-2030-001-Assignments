
<?php

	require_once 'sqlhelper.php';
	require_once '../vendor/autoload.php';  //include the twig library.

	$twig = setupMyTwigEnvironment();

	$conn = connectToMyDatabase();

	if($conn){

		$destructiveTech = $_POST["destructiveTech"];
		$getDestructiveResult = $conn->query("call get_specific_destructive(\"$destructiveTech\")");
          clearConnection($conn);

		$destructiveTestsTable = $getDestructiveResult->fetch_all(MYSQLI_ASSOC);

		$output = array();

		foreach ($destructiveTestsTable as $destructiveTest){

			array_push($output,array(
				"date"=> $destructiveTest["Date"],
				"doctor"=> $destructiveTest["Doctor"],
				"patient"=> $destructiveTest["Patient"],
				"destructive"=> $destructiveTest["Destructive_Technology"],
				"restorative"=> $destructiveTest["Restorative_Technology"],
				"description"=> $destructiveTest["Test_Description"],
				"results"=> $destructiveTest["Results"]));
		}
		echo json_encode($output);

	}else {

		//One benefit is that we can load a full error page
		$template = $twig->load("error.twig.html");
		echo $template->render(array("message"=>"Title query failed"));
	}





?>
