-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2018 at 06:57 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testing_facility`
   CREATE DATABASE `testing_facility`;
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_destructive_log` ()  BEGIN

     SELECT *
     FROM destructive_log;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_doctor_log` ()  BEGIN

     SELECT *
     FROM doctors_log;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_patient_log` ()  BEGIN

     SELECT *
     FROM patients_log;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_restorative_log` ()  BEGIN

     SELECT *
     FROM restorative_log;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_specific_destructive` (IN `destructive` TEXT)  BEGIN

     SELECT *
     FROM test_log
     WHERE test_log.Destructive_Technology = destructive;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_specific_doctor` (IN `doctor` TEXT)  BEGIN

     SELECT *
     FROM test_log
     WHERE test_log.Doctor = doctor;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_specific_patient` (IN `patient` TEXT)  BEGIN

     SELECT *
     FROM test_log
     WHERE test_log.Patient = patient;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_specific_restorative` (IN `restorative` TEXT)  BEGIN

     SELECT *
     FROM test_log
     WHERE test_log.Restorative_Technology = restorative;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_test_log` ()  BEGIN

     SELECT *
     FROM test_log;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_role` (IN `username` TEXT)  BEGIN

     SELECT Role
     FROM users
     WHERE username = users.Username;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `submit_application` (IN `name` TEXT, IN `role` TEXT, IN `message` TEXT)  BEGIN

     INSERT INTO applications (Name, Role, Message)
     VALUES (name, role, message);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `submit_feedback` (IN `name` TEXT, IN `qualifications` TEXT, IN `message` TEXT)  BEGIN

     INSERT INTO feedback (Name, Qualifications, Message)
     VALUES (name, qualifications, message);

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `Name` varchar(255) DEFAULT NULL,
  `Role` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `destructive_log`
--

CREATE TABLE `destructive_log` (
  `ID` varchar(255) DEFAULT NULL,
  `Destructive_Technology` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `destructive_log`
--

INSERT INTO `destructive_log` (`ID`, `Destructive_Technology`) VALUES
('dt0', 'Allcom Memory Eraser'),
('dt1', 'Reign Possession'),
('dt2', 'MIB Neuralizer');

-- --------------------------------------------------------

--
-- Table structure for table `doctors_log`
--

CREATE TABLE `doctors_log` (
  `ID` varchar(255) DEFAULT NULL,
  `Doctor` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctors_log`
--

INSERT INTO `doctors_log` (`ID`, `Doctor`) VALUES
('dr0', 'Rachel Porter'),
('dr1', 'Alex Danvers & Lena Luthor'),
('dr2', 'Michael Jennings');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `Name` varchar(255) DEFAULT NULL,
  `Qualifications` varchar(255) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patients_log`
--

CREATE TABLE `patients_log` (
  `ID` varchar(255) DEFAULT NULL,
  `Patient` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patients_log`
--

INSERT INTO `patients_log` (`ID`, `Patient`) VALUES
('pt0', 'Michael Jennings'),
('pt1', 'Sam Arias');

-- --------------------------------------------------------

--
-- Table structure for table `restorative_log`
--

CREATE TABLE `restorative_log` (
  `ID` varchar(255) DEFAULT NULL,
  `Restorative_Technology` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `restorative_log`
--

INSERT INTO `restorative_log` (`ID`, `Restorative_Technology`) VALUES
('rt0', 'Dark Chocolate'),
('rt1', 'High Fat Diet'),
('rt2', 'Vegan Gluten-Free Diet'),
('rt3', 'Low Dose Electric Shocks'),
('rt4', 'Rhubarb Pie'),
('rt5', 'Rhubarb Root Tea'),
('rt6', 'Crushed Up Rhubarb Root'),
('rt7', 'Rhubarb Root Tablet'),
('rt8', 'Ginger Root Tea'),
('rt9', 'Crushed Up Ginger Root'),
('rt10', 'Ginger Root Tablet'),
('rt11', 'Taro Root Tea'),
('rt12', 'Taro Root Tablet'),
('rt13', 'Wild Flowers'),
('rt14', 'Wild Flower Extract'),
('rt15', 'Green House Flowers'),
('rt16', 'Hydrangea Flower Extract'),
('rt17', 'Herbal Tea'),
('rt18', 'Acquired'),
('rt19', 'Technology Comprehension'),
('rt20', 'First Prototype'),
('rt21', 'Honey Lemon Tea'),
('rt22', 'Hibiscus Tea'),
('rt23', 'Matcha Green Tea');

-- --------------------------------------------------------

--
-- Table structure for table `test_log`
--

CREATE TABLE `test_log` (
  `Date` varchar(19) DEFAULT NULL,
  `Doctor` varchar(255) DEFAULT NULL,
  `Patient` varchar(255) DEFAULT NULL,
  `Destructive_Technology` varchar(255) DEFAULT NULL,
  `Restorative_Technology` varchar(255) DEFAULT NULL,
  `Test_Description` varchar(255) DEFAULT NULL,
  `Results` varchar(360) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_log`
--

INSERT INTO `test_log` (`Date`, `Doctor`, `Patient`, `Destructive_Technology`, `Restorative_Technology`, `Test_Description`, `Results`) VALUES
(DATE_ADD(NOW(), INTERVAL -260 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Dark Chocolate', 'Increased patients intake on dark chocolate.', 'No memory improvement'),
(DATE_ADD(NOW(), INTERVAL -255 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'High Fat Diet', 'Increased patients intake of fat to improve brain activity.', 'Minimal improvement to memories after destructive technology and intense weight gain.'),
(DATE_ADD(NOW(), INTERVAL -225 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Vegan Gluten-Free Diet', 'Put patient on a strict vegan and gluten-free diet to see effect on memory.', 'No memory improvement but patient experienced intense weight loss.'),
(DATE_ADD(NOW(), INTERVAL -200 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Low Dose Electric Shocks', 'Repeatedly shocked the patient with 50,000 volt taser in an effort to provoke memories to surface.', 'Minimal memory improvement and patient in extreme pain.'),
(DATE_ADD(NOW(), INTERVAL -175 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Rhubarb Pie', 'Patient bought a rhubarb pie for their own enjoyment.', 'The patient experienced an improvement to their memory but direct cause is unknown.'),
(DATE_ADD(NOW(), INTERVAL -170 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Rhubarb Root Tea', 'Attempt to recreate effect of rhubarb pie.', 'The patient experienced even more improvement to their memory but memories all seem trivial.'),
(DATE_ADD(NOW(), INTERVAL -160 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Crushed Up Rhubarb Root', 'Attempt to refine the rhubarb effect.', 'The patient experienced long lasting memory recall but all memories are trivial.'),
(DATE_ADD(NOW(), INTERVAL -150 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Rhubarb Root Tablet', 'Refined the rhubarb effect into a tablet.', 'The patient experienced trivial memory recall lasting twenty-four hours.'),
(DATE_ADD(NOW(), INTERVAL -140 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Ginger Root Tea', 'Boiling other roots to see if similar effect is created.', 'The patient experienced sufficient improvement to their memory and memories seem non-trivial.'),
(DATE_ADD(NOW(), INTERVAL -130 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Crushed Up Ginger Root', 'Attempt to refine the ginger effect.', 'The patient experienced short lasting hazy memory recall of significant memories.'),
(DATE_ADD(NOW(), INTERVAL -120 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Ginger Root Tablet', 'Refined the ginger effect into a tablet.', 'The patient experienced hazy recall of significant memories lasting two hours.'),
(DATE_ADD(NOW(), INTERVAL -115 DAY), 'Alex Danvers & Lena Luthor', 'Sam Arias', 'Reign Possession', 'Ginger Root Tablet', 'We were impressed by the previous discovers of this facility and wanted to test the latest technology on our patient.', 'The patient experienced hazy recall of memories lasting ten minutes.'),
(DATE_ADD(NOW(), INTERVAL -110 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Taro Root Tea', 'Boiled taro root to hopefully expand on the effects of previous experiments.', 'The patient experienced unprecedented improvement to their memory and memories vividly significant.'),
(DATE_ADD(NOW(), INTERVAL -109 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Taro Root Tablet', 'Expedited tablet process because of taro root tea results.', 'The patient experienced vivid significant memory recall for ten minutes before collapsing to the floor. Patient remained breathing but unresponsive for fourteen days. When patient woke up their memory was impaired beyond the starting point of the experiment.'),
(DATE_ADD(NOW(), INTERVAL -90 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Wild Flowers', 'Patient went for a walk in the green house after recovering from the side effects of the taro root tablet.', 'The patient smelled some wild flowers in the green house and had some improvement their memories.'),
(DATE_ADD(NOW(), INTERVAL -85 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Wild Flower Extract', 'Extracted the scent of the wild flowers and brought it into the controlled scent lab to be smelled by patient.', 'The patient experienced minimal recall of significant memories for thirty minutes.'),
(DATE_ADD(NOW(), INTERVAL -80 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Green House Flowers', 'Accompanied patient on a walk through the green house to smell all of the available flowers and record memory recall after each.', 'The patient experienced impressive recall of significant memories after smelling the Hydrangea flower.'),
(DATE_ADD(NOW(), INTERVAL -75 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Hydrangea Flower Extract', 'Extracted the scent of the hydrangea flowers and brought it into the controlled scent lab to be smelled by patient.', 'The patient experienced impressive recall of significant memories after smelling the Hydrangea flower. Memory recall lasted for one hour with no side effects.'),
(DATE_ADD(NOW(), INTERVAL -70 DAY), 'Alex Danvers & Lena Luthor', 'Sam Arias', 'Reign Possession', 'Green House Flowers', 'Repeated greenhouse flower smelling experiment for our patient.', 'The patient experienced no significant improvement to memories after smelling any of the flowers.'),
(DATE_ADD(NOW(), INTERVAL -65 DAY), 'Alex Danvers & Lena Luthor', 'Sam Arias', 'Reign Possession', 'Herbal Tea', 'Patient received a herbal tea kit as a gift and took out each tea a home to smell them.', 'That night the patient went to sleep and in their dream state they remembered a significant event that occurred while Reign was in control of their body. Further testing with herbal teas must be conducted.'),
(DATE_ADD(NOW(), INTERVAL -60 DAY), 'Rachel Porter', 'Michael Jennings', 'Allcom Memory Eraser', 'Herbal Tea', 'Repeated herbal tea experiment conducted by Dr. Danvers and Researcher Luthor.', 'The patient experienced no significant recall of memories after smelling all of the different herbal teas.'),
(DATE_ADD(NOW(), INTERVAL -50 DAY), 'Michael Jennings', 'N/A', 'MIB Neuralizer', 'Acquired', 'I was able to get my hands on a MIB Neuralizer and I am going to attempt to reverse engineer it to help people effected by it.', 'No results yet.'),
(DATE_ADD(NOW(), INTERVAL -40 DAY), 'Michael Jennings', 'N/A', 'MIB Neuralizer', 'Technology Comprehension', 'I have spent the past ten days analysing the neuralizer and I believe I understand how to operate it now.', 'I am ready to begin reverse engineering the neuralizer now.'),
(DATE_ADD(NOW(), INTERVAL -10 DAY), 'Michael Jennings', 'N/A', 'MIB Neuralizer', 'First Prototype', 'I have spent a month developing a restorative headset for the neutalizer and I believe I have my first working prototype now.', 'I am ready to begin testing it on patients but we do not have any neuralizer effected patients at the moment.'),
(DATE_ADD(NOW(), INTERVAL -5 DAY), 'Alex Danvers & Lena Luthor', 'Sam Arias', 'Reign Possession', 'Honey Lemon Tea', 'We conducted electroencephalography (EEG) monitoring of the patient\'s brain waves as they slept after drinking the honey lemon tea.', 'The patient\'s EEG was more active than during sleep without tea prior to going to sleep. Additionally, upon awaking the patient was able to recall a significant event with precise detail from when Reign was in control of their actions.'),
(DATE_ADD(NOW(), INTERVAL -3 DAY), 'Alex Danvers & Lena Luthor', 'Sam Arias', 'Reign Possession', 'Hibiscus Tea', 'We conducted electroencephalography (EEG) monitoring of the patient\'s brain waves as they slept after drinking the hibiscus tea.', 'The patient\'s EEG was more active than compared to drinking honey lemon tea prior to going to sleep. Additionally, upon awaking the patient was able to recall multiple significant events with precise detail from when Reign was in control of their actions.'),
(DATE_ADD(NOW(), INTERVAL -1 DAY), 'Alex Danvers & Lena Luthor', 'Sam Arias', 'Reign Possession', 'Matcha Green Tea', 'We conducted electroencephalography (EEG) monitoring of the patient\'s brain waves as they slept after drinking the matcha green tea.', 'The patient\'s EEG was more active than compared to drinking hibiscus tea prior to going to sleep. Additionally, upon awaking the patient was able to recall a week of significant events with precise detail from when Reign was in control of their actions.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Username` varchar(16) DEFAULT NULL,
  `Role` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Username`, `Role`) VALUES
('Rachel Porter', 'Doctor'),
('Michael Jennings', 'Patient'),
('Alex Danvers', 'Doctor'),
('Sam Arias', 'Patient'),
('Lena Luthor', 'Researcher'),
('Guest', 'Guest');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
