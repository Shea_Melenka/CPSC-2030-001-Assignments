
<?php

	require_once 'sqlhelper.php';
	require_once '../vendor/autoload.php';  //include the twig library.

	$twig = setupMyTwigEnvironment();

	$conn = connectToMyDatabase();

	if($conn){

		$name = $_POST["name"];
		$role = $_POST["role"];
		$message = $_POST["message"];

		$submitApplicationResult = $conn->query("call submit_application(\"$name\", \"$role\", \"$message\")");
		clearConnection($conn);

	}else {

		//One benefit is that we can load a full error page
		$template = $twig->load("error.twig.html");
		echo $template->render(array("message"=>"Title query failed"));
	}

?>
