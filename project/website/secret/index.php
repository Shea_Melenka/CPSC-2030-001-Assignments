
<?php

	require_once 'sqlhelper.php';
	require_once '../vendor/autoload.php';  //include the twig library.

	$twig = setupMyTwigEnvironment();

	$conn = connectToMyDatabase();

	if($conn){
		session_start();

		//LOADS THE HEADER, TITLE, AND NAVBAR
		$template = $twig->load('header.twig.html');

		$page = $_GET["page"];
		$lessFileName = "styles.less";
		$userName = $_SESSION["user"];
		$pageName = ucwords($page);

		$pageTitle = array("home"=>"Welcome to Forget Me Not's Memory Testing Facility", "intro"=>"Company Introduction & Mission Statement", "destructive"=>"Types of Destructive Memory Technology", "restorative"=>"Successful Restorative Memory Technology", "testLog"=>"Memory Tests Log", "joinUs"=>"Join Our Team");

		$title1 = $pageTitle[$page];

	     echo $template->render(array("lessFileName"=>$lessFileName, "pageName"=>$pageName, "userName"=>$userName, "title1"=>$title1));

		if($page == "home"){

			$template = $twig->load('homecontent.twig.html');
			echo $template->render(array());

		}elseif($page == "intro"){

			$template = $twig->load('introcontent.twig.html');
			echo $template->render(array());

		}elseif($page == "destructive"){

			$template = $twig->load('destructivecontent.twig.html');
			echo $template->render(array());

		}elseif($page == "restorative"){

			$template = $twig->load('restorativecontent.twig.html');
			echo $template->render(array());

		}elseif($page == "testLog"){

			$testLogResult = $conn->query("call get_test_log()");
			clearConnection($conn);
			$testLog = $testLogResult->fetch_all(MYSQLI_ASSOC);

			$doctorLogResult = $conn->query("call get_doctor_log()");
			clearConnection($conn);
			$doctorLog = $doctorLogResult->fetch_all(MYSQLI_ASSOC);

			$patientLogResult = $conn->query("call get_patient_log()");
			clearConnection($conn);
			$patientLog = $patientLogResult->fetch_all(MYSQLI_ASSOC);

			$destructiveLogResult = $conn->query("call get_destructive_log()");
			clearConnection($conn);
			$destructiveLog = $destructiveLogResult->fetch_all(MYSQLI_ASSOC);

			$restorativeLogResult = $conn->query("call get_restorative_log()");
			clearConnection($conn);
			$restorativeLog = $restorativeLogResult->fetch_all(MYSQLI_ASSOC);

			$template = $twig->load("testlogcontent.twig.html");
		     echo $template->render(array("testlist"=> $testLog, "doctorlist"=> $doctorLog, "patientlist"=> $patientLog, "destructivelist"=> $destructiveLog, "restorativelist"=> $restorativeLog));

		}elseif($page == "joinUs"){

			$template = $twig->load('joinuscontent.twig.html');
			echo $template->render(array());

		}
		
		//LOADS THE FOOTER
		$template = $twig->load('footer.twig.html');

	     echo $template->render(array());

	}else {

		//One benefit is that we can load a full error page
		$template = $twig->load("error.twig.html");
		echo $template->render(array("message"=>"Title query failed"));
	}


?>
