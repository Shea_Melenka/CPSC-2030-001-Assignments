
<?php

	require_once 'sqlhelper.php';
	require_once '../vendor/autoload.php';  //include the twig library.

	$twig = setupMyTwigEnvironment();

	$conn = connectToMyDatabase();

	if($conn){

		$patientName = $_POST["patientName"];
		$getPatientResult = $conn->query("call get_specific_patient(\"$patientName\")");
          clearConnection($conn);

		$patientTestsTable = $getPatientResult->fetch_all(MYSQLI_ASSOC);

		$output = array();

		foreach ($patientTestsTable as $patientTest){

			array_push($output,array(
				"date"=> $patientTest["Date"],
				"doctor"=> $patientTest["Doctor"],
				"patient"=> $patientTest["Patient"],
				"destructive"=> $patientTest["Destructive_Technology"],
				"restorative"=> $patientTest["Restorative_Technology"],
				"description"=> $patientTest["Test_Description"],
				"results"=> $patientTest["Results"]));
		}
		echo json_encode($output);

	}else {

		//One benefit is that we can load a full error page
		$template = $twig->load("error.twig.html");
		echo $template->render(array("message"=>"Title query failed"));
	}





?>
