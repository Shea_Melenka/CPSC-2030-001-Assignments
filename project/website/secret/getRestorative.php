
<?php

	require_once 'sqlhelper.php';
	require_once '../vendor/autoload.php';  //include the twig library.

	$twig = setupMyTwigEnvironment();

	$conn = connectToMyDatabase();

	if($conn){

		$restorativeTech = $_POST["restorativeTech"];
		$getRestorativeResult = $conn->query("call get_specific_restorative(\"$restorativeTech\")");
          clearConnection($conn);

		$restorativeTestsTable = $getRestorativeResult->fetch_all(MYSQLI_ASSOC);

		$output = array();

		foreach ($restorativeTestsTable as $restorativeTest){

			array_push($output,array(
				"date"=> $restorativeTest["Date"],
				"doctor"=> $restorativeTest["Doctor"],
				"patient"=> $restorativeTest["Patient"],
				"destructive"=> $restorativeTest["Destructive_Technology"],
				"restorative"=> $restorativeTest["Restorative_Technology"],
				"description"=> $restorativeTest["Test_Description"],
				"results"=> $restorativeTest["Results"]));
		}
		echo json_encode($output);

	}else {

		//One benefit is that we can load a full error page
		$template = $twig->load("error.twig.html");
		echo $template->render(array("message"=>"Title query failed"));
	}





?>
