
<?php

	require_once 'sqlhelper.php';
	require_once '../vendor/autoload.php';  //include the twig library.

	$twig = setupMyTwigEnvironment();

	$conn = connectToMyDatabase();

	if($conn){

		session_start();
		$_SESSION["user"] = $_POST["username"];

	}else {

		//One benefit is that we can load a full error page
		$template = $twig->load("error.twig.html");
		echo $template->render(array("message"=>"Title query failed"));
	}

?>
