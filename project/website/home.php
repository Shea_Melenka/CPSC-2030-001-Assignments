<!--
	Term Project CPSC2030
	Title: home.php
	Author: Shea Melenka
	Date: October 8th 2018
-->
<?php

	require_once 'sqlhelper.php';
	require_once './vendor/autoload.php';  //include the twig library.

	$twig = setupMyTwigEnvironment();

	$conn = connectToMyDatabase();

	if($conn){

		//LOADS THE HEADER, TITLE, AND NAVBAR
		$template = $twig->load('header.twig.html');

		$lessFileName = "styles.less";
	     $pageName = "Home";
	     $title1 = "Welcome to Forget Me";
	     $title2 = "Not's Nursery";

	     echo $template->render(array("lessFileName"=>$lessFileName, "pageName"=>$pageName, "title1"=>$title1, "title2"=>$title2));



		//LOADS THE BODY
		$template = $twig->load('bodydesigntop.twig.html');

		echo $template->render(array());


		//LOADS THE HOME CONTENT
		$template = $twig->load('homecontent.twig.html');

		echo $template->render(array());


		//LOADS THE BODY
		$template = $twig->load('bodydesignbottom.twig.html');

		echo $template->render(array());



		//LOADS THE FOOTER
		$template = $twig->load('footer.twig.html');

	     echo $template->render(array());

		/*
		//LOADS THE LOGIN
		$template = $twig->load('login.twig.html');

	     echo $template->render(array());
		*/

	}else {

		//One benefit is that we can load a full error page
		$template = $twig->load("error.twig.html");
		echo $template->render(array("message"=>"Title query failed"));
	}





?>
