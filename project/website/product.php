<!--
	Term Project CPSC2030
	Title: product.php
	Author: Shea Melenka
	Date: October 8th 2018
-->
<?php

	require_once 'sqlhelper.php';
	require_once './vendor/autoload.php';  //include the twig library.

	$twig = setupMyTwigEnvironment();

	$conn = connectToMyDatabase();

	if($conn){

		//LOADS THE HEADER, TITLE, AND NAVBAR
		$template = $twig->load('header.twig.html');

		$product = $_GET["product"];

		if($product == "trees"){
			$product = "fruit trees";
		}

		$lessFileName = "styles.less";
		$pageName = ucwords($product);
		$title1 = ucwords($product);
		$title2 = "";

		echo $template->render(array("lessFileName"=>$lessFileName, "pageName"=>$pageName, "title1"=>$title1, "title2"=>$title2));



		//LOADS THE BODY
		$template = $twig->load('bodydesigntop.twig.html');

		echo $template->render(array());

		//LOADS THE PRODUCT CONTENT
		if($product == "flowers"){

			$flowerTableResult = $conn->query("call flower_table()");
			clearConnection($conn);

			if($flowerTableResult){
				$productTable = $flowerTableResult->fetch_all(MYSQLI_ASSOC);

				$template = $twig->load("productList.twig.html");
				echo $template->render(array("productTable"=>$productTable));

			}else{
				echo "ERROR! productList query failed";
			}

		}elseif($product == "fruit trees"){

			$treeTableResult = $conn->query("call tree_table()");
			clearConnection($conn);

			if($treeTableResult){
				$productTable = $treeTableResult->fetch_all(MYSQLI_ASSOC);

				$template = $twig->load("productList.twig.html");
				echo $template->render(array("productTable"=>$productTable));

			}else{
				echo "ERROR! productList query failed";
			}

		}elseif($product == "vegetables"){

			$vegetableTableResult = $conn->query("call vegetable_table()");
			clearConnection($conn);

			if($vegetableTableResult){
				$productTable = $vegetableTableResult->fetch_all(MYSQLI_ASSOC);

				$template = $twig->load("productList.twig.html");
				echo $template->render(array("productTable"=>$productTable));

			}else{
				echo "ERROR! productList query failed";
			}
		}


		//LOADS THE BODY
		$template = $twig->load('bodydesignbottom.twig.html');

		echo $template->render(array());





		//LOADS THE FOOTER
		$template = $twig->load('footer.twig.html');

	     echo $template->render(array());

	}else {

		//One benefit is that we can load a full error page
		$template = $twig->load("error.twig.html");
		echo $template->render(array("message"=>"Title query failed"));
	}


?>
